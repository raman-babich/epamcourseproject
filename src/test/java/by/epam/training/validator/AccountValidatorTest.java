package by.epam.training.validator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

/**
 *
 */
@RunWith(Parameterized.class)
public class AccountValidatorTest {

    private enum TestTarget {

        LOGIN(AccountValidator::validateLogin),
        EMAIL(AccountValidator::validateEmail),
        PASSWORD(AccountValidator::validatePassword);

        private Predicate<String> test;

        TestTarget(Predicate<String> test) {
            this.test = test;
        }

    }

    private String testString;
    private TestTarget target;
    private boolean assertValue;

    public AccountValidatorTest(String testString, TestTarget target, boolean assertValue) {
        this.testString = testString;
        this.target = target;
        this.assertValue = assertValue;
    }

    @Parameterized.Parameters(name = "{index}: {1}({0}) : {2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"adsadasd", TestTarget.LOGIN, true},
                {"ФвыВфв", TestTarget.LOGIN, true},
                {"   ", TestTarget.LOGIN, false},
                {"123", TestTarget.LOGIN, false},
                {"_", TestTarget.LOGIN, false},
                {"a@a.a", TestTarget.EMAIL, true},
                {"@a.a", TestTarget.EMAIL, false},
                {"a@a.", TestTarget.EMAIL, false},
                {"a@.a", TestTarget.EMAIL, false},
                {"asd//..asd@asd,,a.a;sok", TestTarget.EMAIL, true},
                {"1Uuuuu", TestTarget.PASSWORD, true},
                {"asdas", TestTarget.PASSWORD, false},
                {"adsadasd", TestTarget.PASSWORD, false},
                {"ASDADAD", TestTarget.PASSWORD, false},
                {"123143", TestTarget.PASSWORD, false},
        });
    }

    @Test
    public void validateInput() throws Exception {
        Assert.assertEquals(target.test.test(testString), assertValue);
    }
}
