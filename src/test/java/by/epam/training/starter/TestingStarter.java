package by.epam.training.starter;

import by.epam.training.validator.AccountValidatorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 */
@Suite.SuiteClasses({AccountValidatorTest.class})
@RunWith(Suite.class)
public class TestingStarter {

}
