package by.epam.training.controller;

import by.epam.training.command.CommandManager;
import by.epam.training.command.IHttpServletCommand;
import by.epam.training.database.ConnectionPool;
import by.epam.training.entity.Visitor;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.Map;

/**
 *
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/main"})
@MultipartConfig(maxFileSize = 52428800, maxRequestSize = 52428800)//50MB max
public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().closePool();
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        MainServletConfig servletConfig = (MainServletConfig) req.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        Visitor visitor = (Visitor) req.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Map<String, IHttpServletCommand> commands = CommandManager.getCommands(visitor.getRole());
        String resultData = commands.get(servletConfig.getCommand()).execute(req, resp);
        switch (servletConfig.getState()) {
            case FORWARD:
                req.getRequestDispatcher(resultData).forward(req, resp);
                break;
            case REDIRECT:
                resp.sendRedirect(resultData);
                break;
            case RESPONSE:
                resp.getOutputStream().write(Base64.getDecoder().decode(resultData));
                break;
            case AJAX:
                resp.getWriter().write(resultData);
                break;
        }
    }

}
