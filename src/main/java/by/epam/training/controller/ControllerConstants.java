package by.epam.training.controller;

import java.util.Locale;

/**
 *
 */
public final class ControllerConstants {

    public static final String VISITOR_KEY = "visitor";
    public static final String MAIN_SERVLET_CONFIG_KEY = "mainServletConfig";
    public static final String ACCOUNT_KEY = "account";
    public static final String ACCOUNTS_KEY = "accounts";
    public static final String ERROR_KEY = "error";
    public static final String TOPICS_KEY = "topics";
    public static final String CONDUCTED_SURVEY_QUESTIONS_KEY = "conductedSurveyQuestions";
    public static final String CONDUCTED_SURVEY_KEY = "conductedSurvey";
    public static final String NOT_PASSED_SURVEYS_KEY = "notPassedSurveys";
    public static final String PASSED_SURVEYS_KEY = "passedSurveys";
    public static final String ACCESSIBLE_SURVEYS_KEY = "accessibleSurveys";
    public static final String NOT_ACCESSIBLE_SURVEYS_KEY = "notAccessibleSurveys";
    public static final String NOT_AVAILABLE_SURVEYS_KEY = "notAvailableSurveys";
    public static final String PASSED_SURVEY_RESULT_KEY = "passedSurveyResult";
    public static final String PASSED_SURVEY_KEY = "passedSurvey";
    public static final String ACCOUNT_STATISTIC_KEY = "accountStatistic";

    public static final String X_REQUESTED_WITH = "X-Requested-With";
    public static final String AJAX_HEADER_VALUE = "XMLHttpRequest";
    public static final String COMMAND_PARAM = "command";

    public static final Locale DEFAULT_LOCALE = new Locale("en", "US");

}
