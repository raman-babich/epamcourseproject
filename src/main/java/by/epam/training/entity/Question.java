package by.epam.training.entity;

import java.util.Objects;

/**
 *
 */
public class Question implements IDatabaseEntity {

    private int questionId;
    private int surveyId;
    private int topicId;
    private String text;
    private boolean answerType;

    public Question() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return questionId == question.questionId &&
                surveyId == question.surveyId &&
                topicId == question.topicId &&
                answerType == question.answerType &&
                Objects.equals(text, question.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId, surveyId, topicId, text, answerType);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Question{");
        sb.append("questionId=").append(questionId);
        sb.append(", surveyId=").append(surveyId);
        sb.append(", topicId=").append(topicId);
        sb.append(", text='").append(text).append('\'');
        sb.append(", answerType=").append(answerType);
        sb.append('}');
        return sb.toString();
    }

    public boolean isAnswerType() {
        return answerType;
    }

    public void setAnswerType(boolean answerType) {
        this.answerType = answerType;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
