package by.epam.training.entity;

import java.util.Objects;

/**
 *
 */
public class AppLocale implements IDatabaseEntity {

    private int localeId;
    private String name;

    public AppLocale() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppLocale appLocale = (AppLocale) o;
        return localeId == appLocale.localeId &&
                Objects.equals(name, appLocale.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(localeId, name);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AppLocale{");
        sb.append("localeId=").append(localeId);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public int getLocaleId() {
        return localeId;
    }

    public void setLocaleId(int localeId) {
        this.localeId = localeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
