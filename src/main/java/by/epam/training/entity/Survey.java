package by.epam.training.entity;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 */
public class Survey implements IDatabaseEntity {

    private int surveyId;
    private String title;
    private String description;
    private byte[] image;
    private LocalDateTime creatingTime;
    private boolean accessible;

    public Survey() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Survey survey = (Survey) o;
        return surveyId == survey.surveyId &&
                accessible == survey.accessible &&
                Objects.equals(title, survey.title) &&
                Objects.equals(description, survey.description) &&
                Arrays.equals(image, survey.image) &&
                Objects.equals(creatingTime, survey.creatingTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surveyId, title, description, image, creatingTime, accessible);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Survey{");
        sb.append("surveyId=").append(surveyId);
        sb.append(", title='").append(title).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", image=").append(Arrays.toString(image));
        sb.append(", creatingTime=").append(creatingTime);
        sb.append(", accessible=").append(accessible);
        sb.append('}');
        return sb.toString();
    }

    public boolean isAccessible() {
        return accessible;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public LocalDateTime getCreatingTime() {
        return creatingTime;
    }

    public void setCreatingTime(LocalDateTime creatingTime) {
        this.creatingTime = creatingTime;
    }

}
