package by.epam.training.entity;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 */
public class Account implements IDatabaseEntity {

    private int accountId;
    private String firstName;
    private String secondName;
    private String login;
    private String email;
    private String password;
    private byte[] avatar;
    private LocalDateTime creatingTime;
    private boolean admin;
    private int localeId;

    public Account() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountId == account.accountId &&
                admin == account.admin &&
                localeId == account.localeId &&
                Objects.equals(firstName, account.firstName) &&
                Objects.equals(secondName, account.secondName) &&
                Objects.equals(login, account.login) &&
                Objects.equals(email, account.email) &&
                Objects.equals(password, account.password) &&
                Arrays.equals(avatar, account.avatar) &&
                Objects.equals(creatingTime, account.creatingTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, firstName, secondName, login, email, password, avatar, creatingTime, admin, localeId);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Account{");
        sb.append("accountId=").append(accountId);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", secondName='").append(secondName).append('\'');
        sb.append(", login='").append(login).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", avatar=").append(Arrays.toString(avatar));
        sb.append(", creatingTime=").append(creatingTime);
        sb.append(", admin=").append(admin);
        sb.append(", localeId=").append(localeId);
        sb.append('}');
        return sb.toString();
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public LocalDateTime getCreatingTime() {
        return creatingTime;
    }

    public void setCreatingTime(LocalDateTime creatingTime) {
        this.creatingTime = creatingTime;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public int getLocaleId() {
        return localeId;
    }

    public void setLocaleId(int localeId) {
        this.localeId = localeId;
    }

}
