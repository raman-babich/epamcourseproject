package by.epam.training.manager;

import by.epam.training.controller.ControllerConstants;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 */
public class MessageManager {

    private static final Logger LOG = LogManager.getLogger();

    private static final String FILE_PATH = "properties.content";

    private static MessageManager messageManager = new MessageManager();
    private ResourceBundle resourceBundle;
    private Locale currentLocale = ControllerConstants.DEFAULT_LOCALE;

    private MessageManager() {
        try {
            resourceBundle = ResourceBundle.getBundle(FILE_PATH, ControllerConstants.DEFAULT_LOCALE);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, "Can't load content property file.", e);
            throw new RuntimeException("Can't load content property file.");
        }
    }

    public static String getProperty(String key, Locale locale) {
        if (locale != null) {
            if (!messageManager.currentLocale.equals(locale)) {
                messageManager.resourceBundle = ResourceBundle.getBundle(FILE_PATH, locale);
                messageManager.currentLocale = locale;
            }
        } else {
            messageManager.resourceBundle = ResourceBundle.getBundle(FILE_PATH, ControllerConstants.DEFAULT_LOCALE);
            messageManager.currentLocale = ControllerConstants.DEFAULT_LOCALE;
        }
        return messageManager.resourceBundle.getString(key);
    }

}
