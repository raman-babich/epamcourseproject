package by.epam.training.manager;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 */
public class ConfigManager {

    private static final Logger LOG = LogManager.getLogger();

    private static final String FILE_PATH = "properties.configuration";

    private static ConfigManager configManager = new ConfigManager();
    private ResourceBundle resourceBundle;

    private ConfigManager() {
        try {
            resourceBundle = ResourceBundle.getBundle(FILE_PATH);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, "Can't load configuration property file.", e);
            throw new RuntimeException("Can't load configuration property file.");
        }
    }

    public static String getProperty(String key) {
        return configManager.resourceBundle.getString(key);
    }

}
