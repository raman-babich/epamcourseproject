package by.epam.training.command;

import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Survey;
import by.epam.training.entity.Visitor;
import by.epam.training.manager.ImageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

/**
 *
 */
public class LoadImageCommand extends AbstractHttpServletCommand {//TODO: refactor this.

    private static final Logger LOG = LogManager.getLogger();

    private enum ImageSource {
        ACCOUNT, SURVEY
    }

    private static final String ID_PARAM = "id";
    private static final String SOURCE_PARAM = "src";
    private static final String EMPTY_IMAGE = "";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        byte[] image;
        String idString = request.getParameter(ID_PARAM);
        int id = 0;
        String sourceString = request.getParameter(SOURCE_PARAM);
        ImageSource source;
        if (sourceString == null) {
            return EMPTY_IMAGE;
        }
        try {
            source = ImageSource.valueOf(sourceString.toUpperCase());
        } catch (IllegalArgumentException e) {
            return EMPTY_IMAGE;
        }
        if (source == ImageSource.ACCOUNT) {
            image = ImageManager.getDefaultAvatar();
        } else {
            image = ImageManager.getDefaultSurvey();
        }
        if (idString != null) {
            try {
                id = Integer.parseInt(idString);
            } catch (NumberFormatException e) {
                return Base64.getEncoder().encodeToString(image);
            }
        }
        if (source == ImageSource.SURVEY && idString == null) {
            return Base64.getEncoder().encodeToString(image);
        }
        switch (source) {
            case ACCOUNT:
                Account account = (Account) request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
                image = account.getAvatar();
                break;
            case SURVEY:
                int tempId = id;
                Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
                if (visitor.getRole() == Visitor.Role.USER) {
                    List<Survey> notPassedSurveys = (List<Survey>) request.getSession().getAttribute(ControllerConstants.NOT_PASSED_SURVEYS_KEY);
                    List<Survey> passedSurveys = (List<Survey>) request.getSession().getAttribute(ControllerConstants.PASSED_SURVEYS_KEY);
                    Optional<Survey> surveyOptional = notPassedSurveys.stream().filter(s -> s.getSurveyId() == tempId).findFirst();
                    if (surveyOptional.isPresent()) {
                        image = surveyOptional.get().getImage();
                    } else {
                        surveyOptional = passedSurveys.stream().filter(s -> s.getSurveyId() == tempId).findFirst();
                        if (surveyOptional.isPresent()) {
                            image = surveyOptional.get().getImage();
                        }
                    }
                } else {
                    List<Survey> notAccessibleSurveys = (List<Survey>) request.getSession().getAttribute(ControllerConstants.NOT_ACCESSIBLE_SURVEYS_KEY);
                    List<Survey> accessibleSurveys = (List<Survey>) request.getSession().getAttribute(ControllerConstants.ACCESSIBLE_SURVEYS_KEY);
                    Optional<Survey> surveyOptional = accessibleSurveys.stream().filter(s -> s.getSurveyId() == tempId).findFirst();
                    if (surveyOptional.isPresent()) {
                        image = surveyOptional.get().getImage();
                        break;
                    } else {
                        surveyOptional = notAccessibleSurveys.stream().filter(s -> s.getSurveyId() == tempId).findFirst();
                        if (surveyOptional.isPresent()) {
                            image = surveyOptional.get().getImage();
                        }
                    }
                }
                break;
        }
        String resultData;
        if (image == null || image.length == 0) {
            if (source == ImageSource.ACCOUNT) {
                image = ImageManager.getDefaultAvatar();
            } else {
                image = ImageManager.getDefaultSurvey();
            }
        }
        resultData = Base64.getEncoder().encodeToString(image);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        servletConfig.setState(MainServletConfig.State.RESPONSE);
        return resultData;
    }

}
