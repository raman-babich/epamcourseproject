package by.epam.training.command.admin;

import by.epam.training.ajax.AJAXState;
import by.epam.training.ajax.BiTuple;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.SurveyService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class UpdateSurveyAccess extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String ID_PARAM = "id";
    private static final String VALUE_PARAM = "value";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String idString = request.getParameter(ID_PARAM);
        String valueString = request.getParameter(VALUE_PARAM);
        boolean formValid = true;
        int surveyId = 0;
        boolean value = Boolean.parseBoolean(valueString);
        try {
            surveyId = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            formValid = false;
        }
        if (!formValid) {
            LOG.log(Level.WARN, "Update access form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_GUEST_WELCOME), request, response);
        }
        String resultData;
        SurveyService surveyService = new SurveyService();
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_ACCESSIBLE_SURVEY);
        } else {
            try {
                surveyService.updateAccessible(surveyId, value);
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(new BiTuple<>(AJAXState.OK, MessageManager.getProperty(CommandConstants.MESSAGE_OK, visitor.getLocale())));
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during updating access.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

}
