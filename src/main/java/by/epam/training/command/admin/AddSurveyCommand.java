package by.epam.training.command.admin;

import by.epam.training.ajax.AJAXState;
import by.epam.training.ajax.BiTuple;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Answer;
import by.epam.training.entity.Question;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.SurveyService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 */
public class AddSurveyCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String TITLE_PARAM = "surveyTitle";
    private static final String DESCRIPTION_PARAM = "surveyDescription";
    private static final String IMAGE_PARAM = "surveyImage";
    private static final String ACCESSIBLE_PARAM = "surveyAccessible";
    private static final String QUESTION_AMOUNT_PARAM = "surveyQuestionAmount";
    private static final String QUESTION_TEXT_PARAM = "surveyQuestionText-";
    private static final String QUESTION_TOPIC_PARAM = "surveyQuestionTopic-";
    private static final String QUESTION_ANSWER_TYPE_PARAM = "surveyQuestionAnswerType-";
    private static final String QUESTION_ANSWER_AMOUNT_PARAM = "questionAnswerAmount-";
    private static final String QUESTION_ANSWER = "questionAnswer-";

    private static final String QUESTION_ANSWER_TYPE_RADIO = "radio";
    private static final String QUESTION_ANSWER_TYPE_CHECKBOX = "checkbox";
    private static final String QUESTION_ANSWER_INDEX_DELIMITER = "-";
    private static final int MIN_QUESTION_AMOUNT = 1;
    private static final int MIN_ANSWER_AMOUNT = 2;

    private String title;
    private String description;
    private byte[] image;
    private boolean accessible;
    private List<BiTuple<Question, List<Answer>>> surveyQuestions = new ArrayList<>();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        init();
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (!validateForm(request)) {
            LOG.log(Level.WARN, "Add survey form consists null on non-nullable parameters or incorrect formed data.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_ADD_SURVEY), request, response);
        }
        String resultData;
        SurveyService surveyService = new SurveyService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        try {
            if (servletConfig.getState() != MainServletConfig.State.AJAX) {
                resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_ADD_SURVEY);
            } else {
                surveyService.addSurvey(title, description, image, accessible, surveyQuestions);
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(AJAXState.OK, MessageManager.getProperty(CommandConstants.MESSAGE_SURVEY_SUCCESSFULLY_ADDED, visitor.getLocale()));
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, "Errors during add survey.", e);
            resultData = handleDBError(e, request, response);
        }
        return resultData;
    }

    private void init() {
        title = null;
        description = null;
        image = null;
        accessible = false;
        surveyQuestions = new ArrayList<>();
    }

    private boolean validateForm(HttpServletRequest request) {//TODO: need some refactoring.
        boolean formValid = true;
        try {
            title = request.getParameter(TITLE_PARAM);
            description = request.getParameter(DESCRIPTION_PARAM);
            Part imagePart = request.getPart(IMAGE_PARAM);
            image = loadData(imagePart, CommandConstants.MAX_FILE_SIZE);
            String questionAmountString = request.getParameter(QUESTION_AMOUNT_PARAM);
            if (request.getParameter(ACCESSIBLE_PARAM) == null) {
                accessible = false;
            } else {
                accessible = true;
            }
            int questionAmount = Integer.parseInt(questionAmountString);
            formValid = !Stream.of(title, description, imagePart).anyMatch(o -> o == null);
            if (formValid && questionAmount >= MIN_QUESTION_AMOUNT) {
                for (int i = 0; i < questionAmount; ++i) {
                    surveyQuestions.add(new BiTuple<>(new Question(), new ArrayList<>()));
                    Question question = new Question();
                    StringBuilder builder = new StringBuilder(QUESTION_TEXT_PARAM);
                    builder.append(i);
                    question.setText(request.getParameter(builder.toString()));
                    builder = new StringBuilder(QUESTION_TOPIC_PARAM);
                    builder.append(i);
                    question.setTopicId(Integer.parseInt(request.getParameter(builder.toString())));
                    builder = new StringBuilder(QUESTION_ANSWER_AMOUNT_PARAM);
                    builder.append(i);
                    int answerAmount = Integer.parseInt(request.getParameter(builder.toString()));
                    builder = new StringBuilder(QUESTION_ANSWER_TYPE_PARAM);
                    builder.append(i);
                    String answerTypeString = request.getParameter(builder.toString());
                    if (answerTypeString != null) {
                        switch (answerTypeString) {
                            case QUESTION_ANSWER_TYPE_RADIO:
                                question.setAnswerType(true);
                                break;
                            case QUESTION_ANSWER_TYPE_CHECKBOX:
                                question.setAnswerType(false);
                                break;
                            default:
                                formValid = false;
                                break;
                        }
                    } else {
                        formValid = false;
                    }
                    formValid = !Stream.of(question.getText(), question.getTopicId()).anyMatch(o -> o == null);
                    if (!formValid || answerAmount < MIN_ANSWER_AMOUNT) {
                        break;
                    }
                    surveyQuestions.get(i).setLeft(question);
                    for (int j = 0; j < answerAmount; ++j) {
                        builder = new StringBuilder(QUESTION_ANSWER);
                        builder.append(i);
                        builder.append(QUESTION_ANSWER_INDEX_DELIMITER);
                        builder.append(j);
                        Answer answer = new Answer();
                        answer.setText(request.getParameter(builder.toString()));
                        if (answer.getText() == null) {
                            formValid = false;
                            break;
                        }
                        surveyQuestions.get(i).getRight().add(answer);
                    }
                    if (!formValid) {
                        break;
                    }
                }
            }
        } catch (IOException | ServletException | NumberFormatException e) {
            formValid = false;
        }
        return formValid;
    }

}
