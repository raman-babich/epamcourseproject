package by.epam.training.command.admin;

import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.service.TopicService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class EditTopicCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String TOPIC_ID_PARAM = "topicId";
    private static final String TOPIC_NAME_PARAM = "topicName";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String topicIdString = request.getParameter(TOPIC_ID_PARAM);
        String topicName = request.getParameter(TOPIC_NAME_PARAM);
        int topicId = 0;
        boolean formValid = true;
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        try {
            topicId = Integer.parseInt(topicIdString);
        } catch (NumberFormatException e) {
            formValid = false;
        }
        if (topicName == null) {
            formValid = false;
        }
        if (!formValid) {
            LOG.log(Level.WARN, "Change topic form consists null on non-nullable parameters or consists not int value in int parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_EDIT_TOPIC), request, response);
        }
        String resultData;
        TopicService topicService = new TopicService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_EDIT_TOPIC);
            request.setAttribute(TOPIC_ID_PARAM, topicId);
            request.setAttribute(TOPIC_NAME_PARAM, topicName);
        } else {
            try {
                TopicService.Outcome outcome = topicService.updateTopic(topicId, topicName);
                AJAXState state = AJAXState.HANDLE;
                if (outcome == TopicService.Outcome.VALID) {
                    state = AJAXState.OK;
                }
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                List<String> list = new ArrayList<>();
                list.add(TopicService.processOutcome(outcome, visitor.getLocale()));
                list.add(topicIdString);
                list.add(topicName);
                resultData = toJson(state, list);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during change topic name command.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

}
