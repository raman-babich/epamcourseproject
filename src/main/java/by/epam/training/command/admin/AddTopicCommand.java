package by.epam.training.command.admin;

import by.epam.training.ajax.AJAXState;
import by.epam.training.ajax.BiTuple;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Topic;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.service.TopicService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class AddTopicCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String TOPIC_NAME_PARAM = "topicName";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String topicName = request.getParameter(TOPIC_NAME_PARAM);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (topicName == null) {
            LOG.log(Level.WARN, "Add topic form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_ADD_TOPIC), request, response);
        }
        String resultData;
        TopicService topicService = new TopicService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_ADMIN_ADD_TOPIC);
            request.setAttribute(TOPIC_NAME_PARAM, topicName);
        } else {
            try {
                BiTuple<TopicService.Outcome, Topic> outcome = topicService.addTopic(topicName);
                AJAXState state = AJAXState.HANDLE;
                if (outcome.getLeft() == TopicService.Outcome.VALID) {
                    state = AJAXState.OK;
                }
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(state, TopicService.processOutcome(outcome.getLeft(), visitor.getLocale()));
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during add topic.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

}
