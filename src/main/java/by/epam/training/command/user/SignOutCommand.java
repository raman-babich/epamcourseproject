package by.epam.training.command.user;

import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Visitor;
import by.epam.training.manager.ConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class SignOutCommand extends AbstractHttpServletCommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        request.getSession().setAttribute(ControllerConstants.ERROR_KEY, null);
        request.getSession().setAttribute(ControllerConstants.ACCOUNT_KEY, null);
        request.getSession().setAttribute(ControllerConstants.NOT_PASSED_SURVEYS_KEY, null);
        request.getSession().setAttribute(ControllerConstants.PASSED_SURVEYS_KEY, null);
        request.getSession().setAttribute(ControllerConstants.NOT_ACCESSIBLE_SURVEYS_KEY, null);
        request.getSession().setAttribute(ControllerConstants.ACCESSIBLE_SURVEYS_KEY, null);
        request.getSession().setAttribute(ControllerConstants.CONDUCTED_SURVEY_KEY, null);
        request.getSession().setAttribute(ControllerConstants.CONDUCTED_SURVEY_QUESTIONS_KEY, null);
        visitor.setRole(Visitor.Role.GUEST);
        servletConfig.setState(MainServletConfig.State.REDIRECT);//TODO: delete all attr from session.
        return request.getContextPath() + ConfigManager.getProperty(CommandConstants.PATH_PAGE_INDEX);
    }

}
