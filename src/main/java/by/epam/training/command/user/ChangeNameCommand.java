package by.epam.training.command.user;

import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 */
public class ChangeNameCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String FIRST_NAME_PARAM = "firstName";
    private static final String SECOND_NAME_PARAM = "secondName";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String firstName = request.getParameter(FIRST_NAME_PARAM);
        String secondName = request.getParameter(SECOND_NAME_PARAM);
        boolean formValid = !Stream.of(firstName, secondName).anyMatch(s -> s == null);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (!formValid) {
            LOG.log(Level.WARN, "Change name form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS), request, response);
        }
        String resultData;
        AccountService accountService = new AccountService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Account account = (Account)request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS);
            request.setAttribute(FIRST_NAME_PARAM, firstName);
            request.setAttribute(SECOND_NAME_PARAM, secondName);
        } else {
            try {
                accountService.updateFirstName(account.getAccountId(), firstName);
                accountService.updateSecondName(account.getAccountId(), secondName);
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                List<String> list = new ArrayList<>();
                list.add(MessageManager.getProperty(CommandConstants.MESSAGE_CHANGES_SUCCESSFULLY_SAVED, visitor.getLocale()));
                list.add(firstName);
                list.add(secondName);
                resultData = toJson(AJAXState.OK, list);
                account.setFirstName(firstName);
                account.setSecondName(secondName);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during changing account name.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }
}
