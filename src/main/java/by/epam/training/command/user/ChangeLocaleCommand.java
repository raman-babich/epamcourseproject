package by.epam.training.command.user;

import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 *
 */
public class ChangeLocaleCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String LOCALE_PARAM = "locale";
    private static final String LOCALE_LINE_DELIMITER = "_";
    private static final int LANGUAGE = 0;
    private static final int COUNTRY = 1;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String localeString = request.getParameter(LOCALE_PARAM);
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        if (localeString == null) {
            LOG.log(Level.WARN, "Locale form consists null on non-nullable parameters.");
            return visitor.getCurrentPage();
        }
        String[] localeInfo = localeString.split(LOCALE_LINE_DELIMITER);
        if (localeInfo.length < 2) {
            return visitor.getCurrentPage();
        }
        Locale chosenLocale = new Locale(localeInfo[LANGUAGE], localeInfo[COUNTRY]);
        String nextPage = visitor.getCurrentPage();
        AccountService accountService = new AccountService();
        if (visitor.getRole() != Visitor.Role.GUEST) {
            try {
                Account account = (Account) request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
                account.setLocaleId(accountService.changeLocale(account.getAccountId(), chosenLocale));
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during change locale command.", e);
                nextPage = handleDBError(e, MessageManager.getProperty(CommandConstants.MESSAGE_ERROR_LOCALE_TO_DO, visitor.getLocale()), request, response);
            }
        }
        visitor.setLocale(chosenLocale);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        servletConfig.setState(MainServletConfig.State.REDIRECT);
        return request.getContextPath() + nextPage;
    }

}
