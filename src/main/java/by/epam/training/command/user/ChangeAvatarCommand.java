package by.epam.training.command.user;

import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 *
 */
public class ChangeAvatarCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String AVATAR_PARAM = "avatar";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        boolean formValid = true;
        Part avatarPart = null;
        try {
            avatarPart = request.getPart(AVATAR_PARAM);
        } catch (IOException | ServletException e) {
            formValid = false;
        }
        if (avatarPart == null || !formValid) {
            LOG.log(Level.WARN, "Change avatar form is invalid.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS), request, response);
        }
        byte[] avatar;
        avatar = loadData(avatarPart, CommandConstants.MAX_FILE_SIZE);
        String resultData;
        AccountService accountService = new AccountService();
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Account account = (Account)request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS);
        } else {
            try {
                AJAXState state = AJAXState.HANDLE;
                String message;
                if (avatar.length == 0) {
                    message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_IMAGE, visitor.getLocale());
                } else {
                    accountService.updateAvatar(account.getAccountId(), avatar);
                    state = AJAXState.OK;
                    message = MessageManager.getProperty(CommandConstants.MESSAGE_CHANGES_SUCCESSFULLY_SAVED, visitor.getLocale());
                }
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(state, message);
                account.setAvatar(avatar);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during changing account avatar.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

}
