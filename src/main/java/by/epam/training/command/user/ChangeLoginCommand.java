package by.epam.training.command.user;

import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 */
public class ChangeLoginCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String LOGIN_PARAM = "login";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter(LOGIN_PARAM);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (login == null) {
            LOG.log(Level.WARN, "Change login form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS), request, response);
        }
        String resultData;
        AccountService accountService = new AccountService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Account account = (Account)request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS);
            request.setAttribute(LOGIN_PARAM, login);
        } else {
            try {
                AccountService.Outcome outcome = accountService.updateLogin(account.getAccountId(), login);
                AJAXState state = AJAXState.HANDLE;
                if (outcome == AccountService.Outcome.VALID) {
                    state = AJAXState.OK;
                }
                List<String> list = new ArrayList<>();
                list.add(processOutcome(outcome, visitor.getLocale()));
                list.add(login);
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(state, list);
                account.setLogin(login);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during changing account login.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

    private String processOutcome(AccountService.Outcome outcome, Locale locale) {
        String message = "";
        switch (outcome) {
            case VALID:
                message = MessageManager.getProperty(CommandConstants.MESSAGE_CHANGES_SUCCESSFULLY_SAVED, locale);
                break;
            case INVALID_LOGIN:
                message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_LOGIN, locale);
                break;
            case LOGIN_NOT_UNIQUE:
                message = MessageManager.getProperty(CommandConstants.MESSAGE_LOGIN_NOT_UNIQUE, locale);
                break;
        }
        return message;
    }

}
