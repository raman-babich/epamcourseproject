package by.epam.training.command.user;

import by.epam.training.ajax.BiTuple;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.entity.Account;
import by.epam.training.entity.Answer;
import by.epam.training.entity.Question;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.service.AnswerService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class PassSurveyCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String ANSWER_PARAM = "answer-";
    private static final String ANSWER_INDEX_DELIMITER = "-";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        List<BiTuple<Question, List<Answer>>> surveyQuestions = (List<BiTuple<Question, List<Answer>>>) request.getSession().getAttribute(ControllerConstants.CONDUCTED_SURVEY_QUESTIONS_KEY);
        List<Integer> answerIdList = new ArrayList<>();
        StringBuilder builder;
        int questionAmount = surveyQuestions.size();
        try {
            for (int i = 0; i < questionAmount; ++i) {
                Question question = surveyQuestions.get(i).getLeft();
                List<Answer> answers = surveyQuestions.get(i).getRight();
                int answerAmount = answers.size();
                if (question.isAnswerType()) {
                    builder = new StringBuilder(ANSWER_PARAM);
                    builder.append(question.getQuestionId());
                    answerIdList.add(Integer.parseInt(request.getParameter(builder.toString())));
                } else {
                    for (int j = 0; j < answerAmount; ++j) {
                        builder = new StringBuilder(ANSWER_PARAM);
                        builder.append(question.getQuestionId());
                        builder.append(ANSWER_INDEX_DELIMITER);
                        builder.append(j);
                        String idString = request.getParameter(builder.toString());
                        if (idString != null) {
                            answerIdList.add(Integer.parseInt(idString));
                        }
                    }
                }
            }
        } catch (NumberFormatException e) {
            LOG.log(Level.WARN, "Pass survey form consists null on non-nullable parameters or incorrect formed data.");
        }
        String resultData = suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_MAIN), request, response);
        Account account = (Account) request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
        AnswerService answerService = new AnswerService();
        try {
            answerService.commitUserAnswer(answerIdList, account.getAccountId());
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, "Errors during  topic.", e);
            resultData = handleDBError(e, request, response);
        }
        return resultData;
    }

}
