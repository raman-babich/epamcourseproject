package by.epam.training.command.user;

import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class ResetAvatarCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String resultData;
        AccountService accountService = new AccountService();
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Account account = (Account)request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS);
        } else {
            try {
                accountService.updateAvatar(account.getAccountId(), null);
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(AJAXState.OK, MessageManager.getProperty(CommandConstants.MESSAGE_CHANGES_SUCCESSFULLY_SAVED, visitor.getLocale()));
                account.setAvatar(null);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during changing account name.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

}
