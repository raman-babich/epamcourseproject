package by.epam.training.command.user;

import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

/**
 *
 */
public class ChangePasswordCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String OLD_PASSWORD_PARAM = "oldPassword";
    private static final String NEW_PASSWORD_PARAM = "newPassword";
    private static final String REPEATED_NEW_PASSWORD_PARAM = "repeatedNewPassword";

    private class ChangePasswordResult {
        private String message;
        private List<String> invalidParams = new ArrayList<>();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String oldPassword = request.getParameter(OLD_PASSWORD_PARAM);
        String newPassword = request.getParameter(NEW_PASSWORD_PARAM);
        String repeatedNewPassword = request.getParameter(REPEATED_NEW_PASSWORD_PARAM);
        boolean formValid = !Stream.of(oldPassword, newPassword, repeatedNewPassword).anyMatch(s -> s == null);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (!formValid) {
            LOG.log(Level.WARN, "Change password form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS), request, response);
        }
        String resultData;
        AccountService accountService = new AccountService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Account account = (Account)request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_PROFILE_SETTINGS);
            request.setAttribute(NEW_PASSWORD_PARAM, newPassword);
        } else {
            try {
                AccountService.Outcome outcome = accountService.updatePassword(account.getAccountId(), oldPassword, newPassword, repeatedNewPassword);
                AJAXState state = AJAXState.HANDLE;
                if (outcome == AccountService.Outcome.VALID) {
                    state = AJAXState.OK;
                }
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(state, processOutcome(outcome, visitor.getLocale()));
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during changing account password.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

    private ChangePasswordResult processOutcome(AccountService.Outcome outcome, Locale locale) {
        ChangePasswordResult result = new ChangePasswordResult();
        switch (outcome) {
            case VALID:
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_CHANGES_SUCCESSFULLY_SAVED, locale);
                break;
            case INVALID_PASSWORD:
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_PASSWORD, locale);
                result.invalidParams.add(OLD_PASSWORD_PARAM);
                break;
            case INCORRECT_PASSWORD:
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_INCORRECT_PASSWORD, locale);
                result.invalidParams.add(OLD_PASSWORD_PARAM);
                break;
            case INVALID_NEW_PASSWORD:
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_NEW_PASSWORD, locale);
                result.invalidParams.add(NEW_PASSWORD_PARAM);
                break;
            case PASSWORDS_NOT_EQUALS:
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_PASSWORDS_NOT_EQUALS, locale);
                result.invalidParams.add(NEW_PASSWORD_PARAM);
                result.invalidParams.add(REPEATED_NEW_PASSWORD_PARAM);
                break;
        }
        return result;
    }

}
