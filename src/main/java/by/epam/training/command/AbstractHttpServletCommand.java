package by.epam.training.command;

import by.epam.training.ajax.AJAXState;
import by.epam.training.ajax.BiTuple;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Visitor;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 *
 */
public abstract class AbstractHttpServletCommand implements IHttpServletCommand {

    private static Gson gson;

    public static String toJson(BiTuple<AJAXState, ?> data) {
        if (gson == null) {
            gson = new Gson();
        }
        return gson.toJson(data);
    }

    public static String toJson(AJAXState state, Object data) {
        return toJson(new BiTuple<>(state, data));
    }

    public static String handleError(ErrorHolder holder, HttpServletRequest request, HttpServletResponse response) {
        String resultData;
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (servletConfig.getState() == MainServletConfig.State.AJAX) {
            response.setContentType(CommandConstants.MIME_TYPE_JSON);
            return toJson(AJAXState.ERROR, holder);
        } else {
            request.getSession().setAttribute(ControllerConstants.ERROR_KEY, holder);
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_ERROR_ERROR);
        }
        return resultData;
    }

    public static String handleDBError(Exception e,  HttpServletRequest request, HttpServletResponse response) {
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        ErrorHolder errorHolder = new ErrorHolder();
        errorHolder.setCauseMessage(MessageManager.getProperty(CommandConstants.MESSAGE_ERROR_DATABASE_CAUSE, visitor.getLocale()));
        errorHolder.setToDoMessage(MessageManager.getProperty(CommandConstants.MESSAGE_ERROR_DATABASE_TO_DO, visitor.getLocale()));
        errorHolder.setCurrentPage(visitor.getCurrentPage());
        errorHolder.setException(e);
        return handleError(errorHolder, request, response);
    }

    public static String handleDBError(Exception e, String toDoMessage, HttpServletRequest request, HttpServletResponse response) {
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        ErrorHolder errorHolder = new ErrorHolder();
        errorHolder.setCauseMessage(MessageManager.getProperty(CommandConstants.MESSAGE_ERROR_DATABASE_CAUSE, visitor.getLocale()));
        errorHolder.setToDoMessage(toDoMessage);
        errorHolder.setCurrentPage(visitor.getCurrentPage());
        errorHolder.setException(e);
        return handleError(errorHolder, request, response);
    }

    public String suitablePageForm(String uri, HttpServletRequest request, HttpServletResponse response) {
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (servletConfig.getState() == MainServletConfig.State.AJAX) {
            response.setContentType(CommandConstants.MIME_TYPE_JSON);
            return toJson(AJAXState.LOCATION_GO, request.getContextPath() + uri);
        } else {
            return uri;
        }
    }

    public byte[] loadData(Part part, int maxSize) {
        byte[] empty = {};
        byte[] data = empty;
        if (part == null) {
            return empty;
        }
        int fileSize = (int)part.getSize();
        if (fileSize != 0 && fileSize <= maxSize) {
            data = new byte[fileSize];
            try {
                int bytesAmount = part.getInputStream().read(data, 0, fileSize);
                if (bytesAmount != fileSize) {
                    data = empty;
                }
            } catch (IOException e) {
                data = empty;
            }

        }
        return data;
    }

}
