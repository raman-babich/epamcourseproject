package by.epam.training.command.guest;


import by.epam.training.ajax.AJAXState;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

/**
 *
 */
public class SignUpCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String FIRST_NAME_PARAM = "firstName";
    private static final String SECOND_NAME_PARAM = "secondName";
    private static final String LOGIN_PARAM = "login";
    private static final String EMAIL_PARAM = "email";
    private static final String PASSWORD_PARAM = "signUpPassword";
    private static final String REPEATED_PASSWORD_PARAM = "repeatedSignUpPassword";

    private class SignUpResult {
        private List<String> invalidParams = new ArrayList<>();
        private String message = "";
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String firstName = request.getParameter(FIRST_NAME_PARAM);
        String secondName = request.getParameter(SECOND_NAME_PARAM);
        String login = request.getParameter(LOGIN_PARAM);
        String email = request.getParameter(EMAIL_PARAM);
        String password = request.getParameter(PASSWORD_PARAM);
        String repeatedPassword = request.getParameter(REPEATED_PASSWORD_PARAM);
        boolean formValid = !Stream.of(firstName, secondName, login, email, password, repeatedPassword).anyMatch(s -> s == null);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (!formValid) {
            LOG.log(Level.WARN, "Sign up form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_GUEST_WELCOME), request, response);
        }
        String resultData;
        AccountService accountService = new AccountService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        try {
            if (servletConfig.getState() == MainServletConfig.State.AJAX) {
                AccountService.Outcome outcome = accountService.signUp(firstName, secondName, login, email, password, repeatedPassword, visitor.getLocale());
                SignUpResult signUpResult = processOutcome(outcome, visitor.getLocale());
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                AJAXState state = AJAXState.HANDLE;
                if (outcome == AccountService.Outcome.VALID) {
                    state = AJAXState.OK;
                }
                resultData = toJson(state, signUpResult);
            } else {
                resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_GUEST_WELCOME);
                request.setAttribute(FIRST_NAME_PARAM, firstName);
                request.setAttribute(SECOND_NAME_PARAM, secondName);
                request.setAttribute(LOGIN_PARAM, login);
                request.setAttribute(EMAIL_PARAM, email);
                request.setAttribute(PASSWORD_PARAM, password);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, "Errors during sign up guest.", e);
            resultData = handleDBError(e, request, response);
        }
        return resultData;
    }

    private SignUpResult processOutcome(AccountService.Outcome outcome, Locale locale) {
        SignUpResult result = new SignUpResult();
        switch (outcome) {
            case VALID:
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_SIGN_UP_VALID, locale);
                break;
            case INVALID_LOGIN:
                result.invalidParams.add(LOGIN_PARAM);
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_LOGIN, locale);
                break;
            case INVALID_EMAIL:
                result.invalidParams.add(EMAIL_PARAM);
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_EMAIL, locale);
                break;
            case INVALID_PASSWORD:
                result.invalidParams.add(PASSWORD_PARAM);
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_INVALID_PASSWORD, locale);
                break;
            case PASSWORDS_NOT_EQUALS:
                result.invalidParams.add(PASSWORD_PARAM);
                result.invalidParams.add(REPEATED_PASSWORD_PARAM);
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_PASSWORDS_NOT_EQUALS, locale);
                break;
            case LOGIN_NOT_UNIQUE:
                result.invalidParams.add(LOGIN_PARAM);
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_LOGIN_NOT_UNIQUE, locale);
                break;
            case EMAIL_NOT_UNIQUE:
                result.invalidParams.add(EMAIL_PARAM);
                result.message = MessageManager.getProperty(CommandConstants.MESSAGE_EMAIL_NOT_UNIQUE, locale);
                break;
        }
        return result;
    }

}
