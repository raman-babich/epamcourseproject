package by.epam.training.command.guest;

import by.epam.training.ajax.AJAXState;
import by.epam.training.ajax.BiTuple;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Account;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.ConfigManager;
import by.epam.training.manager.MessageManager;
import by.epam.training.service.AccountService;
import by.epam.training.service.LocaleService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Stream;

/**
 *
 */
public class SignInCommand extends AbstractHttpServletCommand {

    private static final Logger LOG = LogManager.getLogger();

    private static final String EMAIL_OR_LOGIN_PARAM = "emailOrLogin";
    private static final String PASSWORD_PARAM = "signInPassword";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String emailOrLogin = request.getParameter(EMAIL_OR_LOGIN_PARAM);
        String password = request.getParameter(PASSWORD_PARAM);
        boolean formValid = !Stream.of(emailOrLogin, password).anyMatch(s -> s == null);
        MainServletConfig servletConfig = (MainServletConfig)request.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (!formValid) {
            LOG.log(Level.WARN, "Sign in form consists null on non-nullable parameters.");
            return suitablePageForm(ConfigManager.getProperty(CommandConstants.PATH_PAGE_GUEST_WELCOME), request, response);
        }
        String resultData;
        AccountService accountService = new AccountService();
        Visitor visitor = (Visitor)request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        if (servletConfig.getState() != MainServletConfig.State.AJAX) {
            resultData = ConfigManager.getProperty(CommandConstants.PATH_PAGE_GUEST_WELCOME);
            request.setAttribute(EMAIL_OR_LOGIN_PARAM, emailOrLogin);
            request.setAttribute(PASSWORD_PARAM, password);
        } else {
            try {
                BiTuple<AJAXState, Object> data;
                if (accountService.signIn(emailOrLogin, password)) {
                    Account account = accountService.loadAccount(emailOrLogin);
                    if (account.isAdmin()) {
                        visitor.setRole(Visitor.Role.ADMIN);
                    } else {
                        visitor.setRole(Visitor.Role.USER);
                    }
                    request.getSession().setAttribute(ControllerConstants.ACCOUNT_KEY, account);
                    visitor.setLocale(new LocaleService().findLocale(account.getLocaleId()));
                    data = new BiTuple<>(AJAXState.LOCATION_REDIRECT, request.getContextPath() + ConfigManager.getProperty(CommandConstants.PATH_PAGE_USER_MAIN));
                } else {
                    data = new BiTuple<>(AJAXState.HANDLE, MessageManager.getProperty(CommandConstants.MESSAGE_SIGN_IN_INVALID, visitor.getLocale()));
                }
                response.setContentType(CommandConstants.MIME_TYPE_JSON);
                resultData = toJson(data);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, "Errors during sign in guest.", e);
                resultData = handleDBError(e, request, response);
            }
        }
        return resultData;
    }

}
