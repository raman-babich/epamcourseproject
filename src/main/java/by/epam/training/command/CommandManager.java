package by.epam.training.command;

import by.epam.training.command.admin.*;
import by.epam.training.command.guest.SignInCommand;
import by.epam.training.command.guest.SignUpCommand;
import by.epam.training.command.user.*;
import by.epam.training.entity.Visitor;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class CommandManager {

    private static Map<String, IHttpServletCommand> userCommands = new HashMap<>();
    private static Map<String, IHttpServletCommand> adminCommands = new HashMap<>();
    private static Map<String, IHttpServletCommand> guestCommands = new HashMap<>();

    static {
        IHttpServletCommand signIn = new SignInCommand();
        IHttpServletCommand signUp = new SignUpCommand();
        IHttpServletCommand changeLocale = new ChangeLocaleCommand();
        IHttpServletCommand signOut = new SignOutCommand();
        IHttpServletCommand loadImage = new LoadImageCommand();
        IHttpServletCommand changeName = new ChangeNameCommand();
        IHttpServletCommand changeLogin = new ChangeLoginCommand();
        IHttpServletCommand changeEmail = new ChangeEmailCommand();
        IHttpServletCommand changeAvatar = new ChangeAvatarCommand();
        IHttpServletCommand resetAvatar = new ResetAvatarCommand();
        IHttpServletCommand changePassword = new ChangePasswordCommand();
        IHttpServletCommand addSurvey = new AddSurveyCommand();
        IHttpServletCommand addTopic = new AddTopicCommand();
        IHttpServletCommand changeTopicName = new EditTopicCommand();
        IHttpServletCommand removeTopic = new RemoveTopicCommand();
        IHttpServletCommand passSurvey = new PassSurveyCommand();
        IHttpServletCommand updateAccess = new UpdateSurveyAccess();

        guestCommands.put("signIn", signIn);
        guestCommands.put("signUp", signUp);
        guestCommands.put("changeLocale", changeLocale);

        userCommands.put("changeLocale", changeLocale);
        userCommands.put("loadImage", loadImage);
        userCommands.put("changeName", changeName);
        userCommands.put("changeLogin", changeLogin);
        userCommands.put("changeEmail", changeEmail);
        userCommands.put("changeAvatar", changeAvatar);
        userCommands.put("resetAvatar", resetAvatar);
        userCommands.put("changePassword", changePassword);
        userCommands.put("passSurvey", passSurvey);
        userCommands.put("signOut", signOut);

        adminCommands.put("changeLocale", changeLocale);
        adminCommands.put("loadImage", loadImage);
        adminCommands.put("changeName", changeName);
        adminCommands.put("changeLogin", changeLogin);
        adminCommands.put("changeEmail", changeEmail);
        adminCommands.put("changeAvatar", changeAvatar);
        adminCommands.put("resetAvatar", resetAvatar);
        adminCommands.put("changePassword", changePassword);
        adminCommands.put("addSurvey", addSurvey);
        adminCommands.put("addTopic", addTopic);
        adminCommands.put("editTopic", changeTopicName);
        adminCommands.put("removeTopic", removeTopic);
        adminCommands.put("passSurvey", passSurvey);
        adminCommands.put("updateAccess", updateAccess);
        adminCommands.put("signOut", signOut);
    }

    public static Map<String, IHttpServletCommand> getCommands(Visitor.Role role) {
        Map<String, IHttpServletCommand> result = guestCommands;
        switch (role) {
            case USER:
                result = userCommands;
                break;
            case ADMIN:
                result = adminCommands;
                break;
        }
        return result;
    }

}
