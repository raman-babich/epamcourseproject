package by.epam.training.command;

/**
 *
 */
public final class CommandConstants {

    public static final String MIME_TYPE_JSON = "application/json";
    public static final int MAX_FILE_SIZE = 10_000_000;

    //Pages
    public static final String PATH_PAGE_ROOT = "path.page.root";
    public static final String PATH_PAGE_INDEX = "path.page.index";

    public static final String PATH_PAGE_GUEST_WELCOME = "path.page.guest.welcome";

    public static final String PATH_PAGE_USER_MAIN = "path.page.user.main";
    public static final String PATH_PAGE_USER_PROFILE_SETTINGS = "path.page.user.profile.settings";

    public static final String PATH_PAGE_ADMIN_ADD_SURVEY = "path.page.admin.add.survey";
    public static final String PATH_PAGE_ADMIN_ADD_TOPIC = "path.page.admin.add.topic";
    public static final String PATH_PAGE_ADMIN_EDIT_TOPIC = "path.page.admin.edit.topic";
    public static final String PATH_PAGE_ADMIN_REMOVE_TOPIC = "path.page.admin.remove.topic";
    public static final String PATH_PAGE_ADMIN_TOPIC_LIST = "path.page.admin.topic.list";
    public static final String PATH_PAGE_ADMIN_ACCOUNT_LIST = "path.page.admin.account.list";
    public static final String PATH_PAGE_ADMIN_ACCESSIBLE_SURVEY = "path.page.admin.accessible.surveys";

    public static final String PATH_PAGE_ERROR_ERROR = "path.page.error.error";
    public static final String PATH_PAGE_ERROR_BAD_REQUEST = "path.page.error.bad.request";
    public static final String PATH_PAGE_ERROR_FORBIDDEN = "path.page.error.forbidden";
    public static final String PATH_PAGE_ERROR_NOT_FOUND = "path.page.error.not.found";

    //Messages
    public static final String MESSAGE_SIGN_UP_VALID = "message.sign.up.valid";
    public static final String MESSAGE_INVALID_LOGIN = "message.invalid.login";
    public static final String MESSAGE_INVALID_EMAIL = "message.invalid.email";
    public static final String MESSAGE_INVALID_PASSWORD = "message.invalid.password";
    public static final String MESSAGE_PASSWORDS_NOT_EQUALS = "message.passwords.not.equals";
    public static final String MESSAGE_LOGIN_NOT_UNIQUE = "message.login.not.unique";
    public static final String MESSAGE_EMAIL_NOT_UNIQUE = "message.email.not.unique";
    public static final String MESSAGE_SIGN_IN_INVALID = "message.sign.in.invalid";
    public static final String MESSAGE_OK = "message.ok";
    public static final String MESSAGE_CHANGES_SUCCESSFULLY_SAVED = "message.changes.successfully.saved";
    public static final String MESSAGE_INVALID_NEW_PASSWORD = "message.invalid.new.password";
    public static final String MESSAGE_INCORRECT_PASSWORD = "message.incorrect.password";
    public static final String MESSAGE_INVALID_IMAGE = "message.invalid.image";
    public static final String MESSAGE_TOPIC_SUCCESSFULLY_ADDED = "message.topic.successfully.added";
    public static final String MESSAGE_TOPIC_SUCCESSFULLY_CHANGED = "message.topic.successfully.changed";
    public static final String MESSAGE_TOPIC_SUCCESSFULLY_REMOVED = "message.topic.successfully.removed";
    public static final String MESSAGE_TOPIC_NAME_IS_NOT_UNIQUE = "message.topic.name.is.not.unique";
    public static final String MESSAGE_TOPIC_DEFAULT_ID = "message.topic.default.id";
    public static final String MESSAGE_SURVEY_SUCCESSFULLY_ADDED = "message.survey.successfully.added";


    public static final String MESSAGE_ERROR_DATABASE_CAUSE = "message.error.database.cause";
    public static final String MESSAGE_ERROR_DATABASE_TO_DO = "message.error.database.to.do";
    public static final String MESSAGE_ERROR_LOCALE_TO_DO = "message.error.locale.to.do";

}
