package by.epam.training.dao;

import by.epam.training.entity.Question;
import by.epam.training.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class QuestionDAO extends AbstractDAO {

    private static final String SQL_ADD_QUESTION = "INSERT INTO `questions`(`surveys_survey_id`, `topics_topic_id`, `question_text`, `question_answer_type`) VALUES (?, ?, ?, ?)";
    private static final String SQL_FIND_QUESTIONS_BY_SURVEY_ID = "SELECT `question_id`, `surveys_survey_id`, `topics_topic_id`, `question_text`, `question_answer_type` FROM `questions` WHERE `surveys_survey_id` = ?";
    private static final String SQL_FIND_QUESTIONS_BY_TOPIC_ID = "SELECT `question_id`, `surveys_survey_id`, `topics_topic_id`, `question_text`, `question_answer_type` FROM `questions` WHERE `topics_topic_id` = ?";
    private static final String SQL_UPDATE_SURVEY_ID_BY_ID = "UPDATE `questions` SET `surveys_survey_id` = ? WHERE `question_id` = ?";
    private static final String SQL_UPDATE_TOPIC_ID_BY_ID = "UPDATE `questions` SET `topics_topic_id` = ? WHERE `question_id` = ?";
    private static final String SQL_UPDATE_TEXT_BY_ID = "UPDATE `questions` SET `question_text` = ? WHERE `question_id` = ?";
    private static final String SQL_UPDATE_ANSWER_TYPE_BY_ID = "UPDATE `questions` SET `question_answer_type` = ? WHERE `question_id` = ?";

    private static final String QUESTION_ID_COLUMN = "question_id";
    private static final String SURVEY_ID_COLUMN = "surveys_survey_id";
    private static final String TOPIC_ID_COLUMN = "topics_topic_id";
    private static final String QUESTION_TEXT_COLUMN = "question_text";
    private static final String QUESTION_ANSWER_TYPE = "question_answer_type";

    public QuestionDAO(Connection connection) {
        super(connection);
    }

    public Question addQuestion(int surveyId, int topicId, String questionText, boolean answerType) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_QUESTION, Statement.RETURN_GENERATED_KEYS)) {
            Question question = null;
            statement.setInt(1, surveyId);
            statement.setInt(2, topicId);
            statement.setString(3, questionText);
            statement.setBoolean(4, answerType);
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                question = new Question();
                question.setQuestionId(resultSet.getInt(1));
                question.setSurveyId(surveyId);
                question.setTopicId(topicId);
                question.setText(questionText);
                question.setAnswerType(answerType);
            }
            return question;
        } catch (SQLException e) {
            throw new DAOException("Problems with adding question to database.", e);
        }
    }

    public void updateSurveyIdById(int questionId, int surveyId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_SURVEY_ID_BY_ID)) {
            statement.setInt(1, surveyId);
            statement.setInt(2, questionId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating question survey id to database.", e);
        }
    }

    public void updateTopicIdById(int questionId, int topicId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TOPIC_ID_BY_ID)) {
            statement.setInt(1, topicId);
            statement.setInt(2, questionId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating question topic id to database.", e);
        }
    }

    public void updateTextById(int questionId, String text) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TEXT_BY_ID)) {
            statement.setString(1, text);
            statement.setInt(2, questionId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating question text to database.", e);
        }
    }

    public void updateAnswerTypeById(int questionId, boolean answerType) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ANSWER_TYPE_BY_ID)) {
            statement.setBoolean(1, answerType);
            statement.setInt(2, questionId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating question answer type to database.", e);
        }
    }

    public List<Question> findQuestionsBySurveyId(int surveyId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_QUESTIONS_BY_SURVEY_ID)) {
            List<Question> questions = new ArrayList<>();
            statement.setInt(1, surveyId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                questions.add(initQuestion(resultSet));
            }
            return questions;
        } catch (SQLException e) {
            throw new DAOException("Problems with finding questions by survey id in database.", e);
        }
    }

    public List<Question> findQuestionsByTopicId(int topicId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_QUESTIONS_BY_TOPIC_ID)) {
            List<Question> questions = new ArrayList<>();
            statement.setInt(1, topicId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                questions.add(initQuestion(resultSet));
            }
            return questions;
        } catch (SQLException e) {
            throw new DAOException("Problems with finding questions by topic id in database.", e);
        }
    }

    private Question initQuestion(ResultSet resultSet) throws SQLException {
        Question question = new Question();
        question.setQuestionId(resultSet.getInt(QUESTION_ID_COLUMN));
        question.setSurveyId(resultSet.getInt(SURVEY_ID_COLUMN));
        question.setTopicId(resultSet.getInt(TOPIC_ID_COLUMN));
        question.setText(resultSet.getString(QUESTION_TEXT_COLUMN));
        question.setAnswerType(resultSet.getBoolean(QUESTION_ANSWER_TYPE));
        return question;
    }

}
