package by.epam.training.dao;

import by.epam.training.entity.Survey;
import by.epam.training.exception.DAOException;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SurveyDAO extends AbstractDAO {

    private static final String SQL_ADD_SURVEY = "INSERT INTO `surveys`(`survey_title`, `survey_description`, `survey_image`, `survey_creating_date_time`, `survey_is_accessible`) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_FIND_SURVEYS_BY_ACCESSIBLE = "SELECT `survey_id`, `survey_title`, `survey_description`, `survey_image`, `survey_creating_date_time` FROM `surveys` WHERE `survey_is_available` = TRUE AND `survey_is_accessible` = ?";
    private static final String SQL_FIND_SURVEYS_BY_AVAILABLE = "SELECT `survey_id`, `survey_title`, `survey_description`, `survey_image`, `survey_creating_date_time`, `survey_is_accessible` FROM `surveys` WHERE `survey_is_available` = ?";
    private static final String SQL_UPDATE_TITLE_BY_ID = "UPDATE `surveys` SET `survey_title` = ? WHERE `survey_id` = ?";
    private static final String SQL_UPDATE_DESCRIPTION_BY_ID = "UPDATE `surveys` SET `survey_description` = ? WHERE `survey_id` = ?";
    private static final String SQL_UPDATE_IMAGE_BY_ID = "UPDATE `surveys` SET `survey_image` = ? WHERE `survey_id` = ?";
    private static final String SQL_UPDATE_ACCESSIBLE_BY_ID = "UPDATE `surveys` SET `survey_is_accessible` = ? WHERE `survey_id` = ?";
    private static final String SQL_UPDATE_AVAILABLE_BY_ID = "UPDATE `surveys` SET `survey_is_available` = ? WHERE `survey_id` = ?";
    private static final String SQL_FULL_SURVEY_BY_ID = "SELECT `survey_id`, `survey_title`, `survey_description`, `survey_image`, `survey_creating_date_time`, `survey_is_accessible` FROM `surveys` WHERE `survey_id` = ?";
    private static final String SQL_CHECK_IF_PASSED = "SELECT 1 FROM (SELECT `question_id` FROM `surveys` JOIN `questions` ON `surveys`.`survey_id` = `questions`.`surveys_survey_id` WHERE `survey_id` = ?) as `que`" +
            "JOIN `answers` ON `que`.`question_id` = `answers`.`questions_question_id` " +
            "JOIN `accounts_m2m_answers` ON `answers`.`answer_id` = `accounts_m2m_answers`.`answers_answer_id` WHERE accounts_account_id = ?";

    private static final String SURVEY_ID_COLUMN = "survey_id";
    private static final String SURVEY_TITLE_COLUMN = "survey_title";
    private static final String SURVEY_DESCRIPTION_COLUMN = "survey_description";
    private static final String SURVEY_IMAGE_COLUMN = "survey_image";
    private static final String SURVEY_CREATING_DATE_TIME_COLUMN = "survey_creating_date_time";
    private static final String SURVEY_IS_ACCESSIBLE_COLUMN = "survey_is_accessible";
    private static final String SURVEY_IS_AVAILABLE_COLUMN = "survey_is_available";

    public SurveyDAO(Connection connection) {
        super(connection);
    }

    public Survey addSurvey(String title, String description, byte[] image, boolean accessible) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_SURVEY, Statement.RETURN_GENERATED_KEYS)) {
            Survey survey = null;
            statement.setString(1, title);
            statement.setString(2, description);
            if (image != null  && image.length != 0) {
                statement.setBlob(3, new ByteArrayInputStream(image));
            } else {
                statement.setNull(3, Types.BLOB);
            }
            LocalDateTime localDateTime = LocalDateTime.now();
            statement.setTimestamp(4, Timestamp.valueOf(localDateTime));
            statement.setBoolean(5, accessible);
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                survey = new Survey();
                survey.setSurveyId(resultSet.getInt(1));
                survey.setTitle(title);
                survey.setDescription(description);
                survey.setImage(image);
                survey.setCreatingTime(localDateTime);
                survey.setAccessible(accessible);
            }
            return survey;
        } catch (SQLException e) {
            throw new DAOException("Problems with adding survey to database.", e);
        }
    }

    public boolean checkIfPassedByAccount(int surveyId, int accountId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_IF_PASSED)) {
            statement.setInt(1, surveyId);
            statement.setInt(2, accountId);
            return statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking survey passing by account id to database.", e);
        }
    }

    public Survey findSurveyById(int id) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FULL_SURVEY_BY_ID)) {
            Survey survey = null;
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                survey = new Survey();
                survey.setSurveyId(resultSet.getInt(SURVEY_ID_COLUMN));
                survey.setTitle(resultSet.getString(SURVEY_TITLE_COLUMN));
                survey.setDescription(resultSet.getString(SURVEY_DESCRIPTION_COLUMN));
                Blob image = resultSet.getBlob(SURVEY_IMAGE_COLUMN);
                if (image == null) {
                    survey.setImage(null);
                } else {
                    survey.setImage(image.getBytes(1, (int)image.length()));
                }
                survey.setCreatingTime(resultSet.getTimestamp(SURVEY_CREATING_DATE_TIME_COLUMN).toLocalDateTime());
                survey.setAccessible(resultSet.getBoolean(SURVEY_IS_ACCESSIBLE_COLUMN));
            }
            return survey;
        } catch (SQLException e) {
            throw new DAOException("Problems with finding survey by id in database.", e);
        }
    }

    public void updateTitleById(int id, String title) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TITLE_BY_ID)) {
            statement.setString(1, title);
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating survey title in database.", e);
        }
    }

    public void updateDescriptionById(int id, String description) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_DESCRIPTION_BY_ID)) {
            statement.setString(1, description);
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating survey description in database.", e);
        }
    }

    public void updateImageById(int id, byte[] image) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_IMAGE_BY_ID)) {
            if (image != null  && image.length != 0) {
                statement.setBlob(1, new ByteArrayInputStream(image));
            } else {
                statement.setNull(1, Types.BLOB);
            }
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating survey image in database.", e);
        }
    }

    public void updateAccessibleById(int id, boolean accessible) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ACCESSIBLE_BY_ID)) {
            statement.setBoolean(1, accessible);
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating survey accessibleness in database.", e);
        }
    }

    public void updateAvailableById(int id, boolean available) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_AVAILABLE_BY_ID)) {
            statement.setBoolean(1, available);
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating survey availableness in database.", e);
        }
    }

    public List<Survey> findSurveysByAccessible(boolean accessible) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_SURVEYS_BY_ACCESSIBLE)) {
            List<Survey> surveys = new ArrayList<>();
            statement.setBoolean(1, accessible);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Survey survey = initSurvey(resultSet);
                survey.setAccessible(accessible);
                surveys.add(survey);
            }
            return surveys;
        } catch (SQLException e) {
            throw new DAOException("Problems with loading surveys by accessibleness from database.", e);
        }
    }

    public List<Survey> findSurveysByAvailable(boolean available) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_SURVEYS_BY_AVAILABLE)) {
            List<Survey> surveys = new ArrayList<>();
            statement.setBoolean(1, available);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Survey survey = initSurvey(resultSet);
                survey.setAccessible(resultSet.getBoolean(SURVEY_IS_ACCESSIBLE_COLUMN));
                surveys.add(survey);
            }
            return surveys;
        } catch (SQLException e) {
            throw new DAOException("Problems with loading surveys by availableness from database.", e);
        }
    }

    private Survey initSurvey(ResultSet resultSet) throws SQLException {
        Survey survey = new Survey();
        survey.setSurveyId(resultSet.getInt(SURVEY_ID_COLUMN));
        survey.setTitle(resultSet.getString(SURVEY_TITLE_COLUMN));
        survey.setDescription(resultSet.getString(SURVEY_DESCRIPTION_COLUMN));
        Blob image = resultSet.getBlob(SURVEY_IMAGE_COLUMN);
        if (image == null) {
            survey.setImage(null);
        } else {
            survey.setImage(image.getBytes(1, (int)image.length()));
        }
        survey.setCreatingTime(resultSet.getTimestamp(SURVEY_CREATING_DATE_TIME_COLUMN).toLocalDateTime());
        return survey;
    }

}
