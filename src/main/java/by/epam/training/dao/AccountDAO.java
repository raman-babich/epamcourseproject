package by.epam.training.dao;

import by.epam.training.entity.Account;
import by.epam.training.exception.DAOException;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class AccountDAO extends AbstractDAO {//TODO: handle is_available

    private static final String SQL_ADD_ACCOUNT = "INSERT INTO `accounts` " +
            "(`first_name`, `second_name`, `login`, `email`, `password`, `account_creating_date_time`, `is_admin`, `locales_locale_id`) " +
            "VALUES (?, ?, ?, ?, SHA2(?, 256), ?, FALSE, ?)";
    private static final String SQL_CHECK_PASSWORD_BY_EMAIL_OR_LOGIN = "SELECT 1 FROM `accounts` WHERE (`email` = ? OR `login` = ?) AND `password` = SHA2(?, 256)";
    private static final String SQL_CHECK_PASSWORD_BY_ID = "SELECT 1 FROM `accounts` WHERE `account_id` = ?  AND `password` = SHA2(?, 256)";
    private static final String SQL_CHECK_EMAIL_UNIQUENESS = "SELECT 1 FROM `accounts` WHERE `email` = ?";
    private static final String SQL_CHECK_EMAIL_UNIQUENESS_FOR_CHANGE = "SELECT 1 FROM `accounts` WHERE `account_id` <> ? AND  `email` = ?";
    private static final String SQL_CHECK_LOGIN_UNIQUENESS = "SELECT 1 FROM `accounts` WHERE `login` = ?";
    private static final String SQL_CHECK_LOGIN_UNIQUENESS_FOR_CHANGE = "SELECT 1 FROM `accounts` WHERE `account_id` <> ? AND `login` = ?";
    private static final String SQL_CHECK_ADMIN_PERMISSION = "SELECT 1 FROM `accounts` WHERE (`email` = ? OR `login` = ?) AND `is_admin` = TRUE";
    private static final String SQL_FIND_ACCOUNT_BY_EMAIL_OR_LOGIN = "SELECT `account_id`, `locales_locale_id`, `first_name`, `second_name`, `login`, `email`, `avatar`, `account_creating_date_time`, `is_admin` FROM `accounts` WHERE (`email` = ? OR `login` = ?)";
    private static final String SQL_UPDATE_LOCALE_BY_ACCOUNT_ID = "UPDATE `accounts` SET `locales_locale_id` = ? WHERE `account_id` = ?";
    private static final String SQL_UPDATE_FIRST_NAME_BY_ACCOUNT_ID = "UPDATE `accounts` SET `first_name` = ? WHERE `account_id` = ?";
    private static final String SQL_UPDATE_SECOND_NAME_BY_ACCOUNT_ID = "UPDATE `accounts` SET `second_name` = ? WHERE `account_id` = ?";
    private static final String SQL_UPDATE_LOGIN_BY_ACCOUNT_ID = "UPDATE `accounts` SET `login` = ? WHERE `account_id` = ?";
    private static final String SQL_UPDATE_EMAIL_BY_ACCOUNT_ID = "UPDATE `accounts` SET `email` = ? WHERE `account_id` = ?";
    private static final String SQL_UPDATE_AVATAR_BY_ACCOUNT_ID = "UPDATE `accounts` SET `avatar` = ? WHERE `account_id` = ?";
    private static final String SQL_UPDATE_PASSWORD_BY_ACCOUNT_ID = "UPDATE `accounts` SET `password` = ? WHERE `account_id` = ?";
    private static final String SQL_LIST_ALL = "SELECT `account_id`, `first_name`, `second_name`, `login`, `email`, `password`, `avatar`, `account_creating_date_time`, `is_admin`, `locales_locale_id` FROM `accounts`";

    private static final String ACCOUNT_ID_COLUMN = "account_id";
    private static final String FIRST_NAME_COLUMN = "first_name";
    private static final String SECOND_NAME_COLUMN = "second_name";
    private static final String LOGIN_COLUMN = "login";
    private static final String EMAIL_COLUMN = "email";
    private static final String PASSWORD_COLUMN = "password";
    private static final String AVATAR_COLUMN = "avatar";
    private static final String CREATING_DATE_COLUMN = "account_creating_date_time";
    private static final String IS_ADMIN_COLUMN = "is_admin";
    private static final String ACCOUNT_LOCALE_COLUMN = "locales_locale_id";
    private static final String IS_AVAIlABLE_COLUMN = "is_available";

    public AccountDAO(Connection connection) {
        super(connection);
    }

    public Account addAccount(String firstName, String secondName, String login, String email, String password, int locale) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_ACCOUNT, Statement.RETURN_GENERATED_KEYS)) {
            Account account = null;
            statement.setString(1, firstName);
            statement.setString(2, secondName);
            statement.setString(3, login);
            statement.setString(4, email);
            statement.setString(5, password);
            LocalDateTime localDateTime = LocalDateTime.now();
            statement.setTimestamp(6, Timestamp.valueOf(localDateTime));
            statement.setInt(7, locale);
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                account = new Account();
                account.setAccountId(resultSet.getInt(1));
                account.setFirstName(firstName);
                account.setSecondName(secondName);
                account.setLogin(login);
                account.setEmail(email);
                account.setAvatar(null);
                account.setCreatingTime(localDateTime);
                account.setLocaleId(locale);
                account.setAdmin(false);
            }
            return account;
        } catch (SQLException e) {
            throw new DAOException("Problems with adding account to database.", e);
        }
    }

    public List<Account> listAllAccounts() throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_LIST_ALL)) {
            List<Account> accounts = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Account account = new Account();
                account.setAccountId(resultSet.getInt(ACCOUNT_ID_COLUMN));
                account.setFirstName(resultSet.getString(FIRST_NAME_COLUMN));
                account.setSecondName(resultSet.getString(SECOND_NAME_COLUMN));
                account.setLogin(resultSet.getString(LOGIN_COLUMN));
                account.setEmail(resultSet.getString(EMAIL_COLUMN));
                Blob avatar = resultSet.getBlob(AVATAR_COLUMN);
                if (avatar == null) {
                    account.setAvatar(null);
                } else {
                    account.setAvatar(avatar.getBytes(1, (int)avatar.length()));
                }
                account.setCreatingTime(resultSet.getTimestamp(CREATING_DATE_COLUMN).toLocalDateTime());
                account.setAdmin(resultSet.getBoolean(IS_ADMIN_COLUMN));
                account.setLocaleId(resultSet.getInt(ACCOUNT_LOCALE_COLUMN));
                accounts.add(account);
            }
            return accounts;
        } catch (SQLException e) {
            throw new DAOException("Problems with checking account existence in database.", e);
        }
    }

    public boolean checkPassword(String emailOrLogin, String password) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_PASSWORD_BY_EMAIL_OR_LOGIN)) {
            statement.setString(1, emailOrLogin);
            statement.setString(2, emailOrLogin);
            statement.setString(3, password);
            return statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking account existence in database.", e);
        }
    }

    public boolean checkPassword(int accountId, String password) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_PASSWORD_BY_ID)) {
            statement.setInt(1, accountId);
            statement.setString(2, password);
            return statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking account existence in database.", e);
        }
    }

    public boolean checkEmailUniqueness(String email) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_EMAIL_UNIQUENESS)) {
            statement.setString(1, email);
            return !statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking email uniqueness in database.", e);
        }
    }

    public boolean checkEmailUniqueness(int accountId, String email) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_EMAIL_UNIQUENESS_FOR_CHANGE)) {
            statement.setInt(1, accountId);
            statement.setString(2, email);
            return !statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking email uniqueness in database.", e);
        }
    }

    public boolean checkLoginUniqueness(String login) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_LOGIN_UNIQUENESS)) {
            statement.setString(1, login);
            return !statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking login uniqueness in database.", e);
        }
    }

    public boolean checkLoginUniqueness(int accountId, String login) throws DAOException {//TODO: what about javadoc?
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_LOGIN_UNIQUENESS_FOR_CHANGE)) {
            statement.setInt(1, accountId);
            statement.setString(2, login);
            return !statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking login uniqueness in database.", e);
        }
    }

    public boolean checkAdminPermission(String emailOrLogin) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_ADMIN_PERMISSION)) {
            statement.setString(1, emailOrLogin);
            statement.setString(2, emailOrLogin);
            return statement.executeQuery().next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking admin permission in database.", e);
        }
    }

    public Account findAccountByEmailOrLogin(String emailOrLogin) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_EMAIL_OR_LOGIN)) {
            statement.setString(1, emailOrLogin);
            statement.setString(2, emailOrLogin);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Account result = new Account();
                result.setAccountId(resultSet.getInt(ACCOUNT_ID_COLUMN));
                result.setFirstName(resultSet.getString(FIRST_NAME_COLUMN));
                result.setSecondName(resultSet.getString(SECOND_NAME_COLUMN));
                result.setLogin(resultSet.getString(LOGIN_COLUMN));
                result.setEmail(resultSet.getString(EMAIL_COLUMN));
                Blob avatar = resultSet.getBlob(AVATAR_COLUMN);
                if (avatar == null) {
                    result.setAvatar(null);
                } else {
                    result.setAvatar(avatar.getBytes(1, (int)avatar.length()));
                }
                result.setCreatingTime(resultSet.getTimestamp(CREATING_DATE_COLUMN).toLocalDateTime());
                result.setAdmin(resultSet.getBoolean(IS_ADMIN_COLUMN));
                result.setLocaleId(resultSet.getInt(ACCOUNT_LOCALE_COLUMN));
                return result;
            }
            else {
                throw new DAOException("Can't find account by email or login in database.");
            }

        } catch (SQLException e) {
            throw new DAOException("Problems with finding account by email or login in database.", e);
        }
    }

    public void updateFirstNameByAccountId(int accountId, String firstName) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_FIRST_NAME_BY_ACCOUNT_ID)) {
            statement.setString(1, firstName);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating first name by account id in database.", e);
        }
    }

    public void updateSecondNameByAccountId(int accountId, String secondName) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_SECOND_NAME_BY_ACCOUNT_ID)) {
            statement.setString(1, secondName);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating first name by account id in database.", e);
        }
    }

    public void updateLoginByAccountId(int accountId, String login) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_LOGIN_BY_ACCOUNT_ID)) {
            statement.setString(1, login);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating first name by account id in database.", e);
        }
    }

    public void updateEmailByAccountId(int accountId, String email) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_EMAIL_BY_ACCOUNT_ID)) {
            statement.setString(1, email);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating first name by account id in database.", e);
        }
    }

    public void updateAvatarByAccountId(int accountId, byte[] avatar) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_AVATAR_BY_ACCOUNT_ID)) {
            if (avatar != null && avatar.length != 0) {
                statement.setBlob(1, new ByteArrayInputStream(avatar));
            } else {
                statement.setNull(1, Types.BLOB);
            }
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating first name by account id in database.", e);
        }
    }

    public void updatePasswordByAccountId(int accountId, String password) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PASSWORD_BY_ACCOUNT_ID)) {
            statement.setString(1, password);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating first name by account id in database.", e);
        }
    }

    public void updateLocaleByAccountId(int accountId, int localeId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_LOCALE_BY_ACCOUNT_ID)) {
            statement.setInt(1, localeId);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating locale by account id in database.", e);
        }
    }

}
