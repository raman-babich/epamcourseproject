package by.epam.training.dao;

import by.epam.training.entity.Topic;
import by.epam.training.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class TopicDAO extends AbstractDAO {

    private static final String SQL_ADD_TOPIC = "INSERT INTO `topics`(`topic_name`) VALUE(?)";
    private static final String SQL_LIST_ALL_TOPICS = "SELECT `topic_id`, `topic_name` FROM `topics`";
    private static final String SQL_REMOVE_TOPIC_BY_ID = "DELETE FROM `topics` WHERE `topic_id` = ?";
    private static final String SQL_UPDATE_TOPIC_NAME_BY_ID = "UPDATE `topics` SET `topic_name` = ? WHERE `topic_id` = ?";
    private static final String SQL_CHECK_TOPIC_NAME_UNIQUENESS = "SELECT 1 FROM `topics` WHERE `topic_name` = ?";

    private static final String TOPIC_ID_COLUMN = "topic_id";
    private static final String TOPIC_NAME_COLUMN = "topic_name";

    public TopicDAO(Connection connection) {
        super(connection);
    }

    public Topic addTopic(String name) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_TOPIC, Statement.RETURN_GENERATED_KEYS)) {
            Topic topic = null;
            statement.setString(1, name);
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                topic = new Topic();
                topic.setTopicId(resultSet.getInt(1));
                topic.setName(name);
            }
            return topic;
        } catch (SQLException e) {
            throw new DAOException("Problems with adding topic to database.", e);
        }
    }

    public List<Topic> listAllTopics() throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_LIST_ALL_TOPICS)) {
            List<Topic> topics = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Topic topic = new Topic();
                topic.setTopicId(resultSet.getInt(TOPIC_ID_COLUMN));
                topic.setName(resultSet.getString(TOPIC_NAME_COLUMN));
                topics.add(topic);
            }
            return topics;
        } catch (SQLException e) {
            throw new DAOException("Problems with retrieving topic from database.", e);
        }
    }

    public void removeTopicById(int id) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_REMOVE_TOPIC_BY_ID)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with adding topic to database.", e);
        }
    }

    public void updateTopicNameById(int id, String name) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TOPIC_NAME_BY_ID)) {
            statement.setString(1, name);
            statement.setInt(2, id);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating topic name in database.", e);
        }
    }

    public boolean checkTopicNameUniqueness(String name) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_CHECK_TOPIC_NAME_UNIQUENESS)) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            return !resultSet.next();
        } catch (SQLException e) {
            throw new DAOException("Problems with checking topic name for uniqueness in database.", e);
        }
    }

}
