package by.epam.training.dao;

import by.epam.training.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;

/**
 *
 */
public class AppLocaleDAO extends AbstractDAO {

    private static final String SQL_FIND_LOCALE_NAME_BY_LOCALE_ID = "SELECT `locale_name` FROM `locales` WHERE `locale_id` = ?";
    private static final String SQL_FIND_LOCALE_ID_BY_LOCALE_NAME = "SELECT `locale_id` FROM `locales` WHERE `locale_name` = ?";

    private static final String LOCALE_ID_COLUMN = "locale_id";
    private static final String LOCALE_NAME_COLUMN = "locale_name";

    private static final String LOCALE_LINE_DELIMITER = "_";
    private static final int LANGUAGE = 0;
    private static final int COUNTRY = 1;

    public AppLocaleDAO(Connection connection) {
        super(connection);
    }

    public Locale findLocaleByLocaleId(int localeId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_LOCALE_NAME_BY_LOCALE_ID)) {
            statement.setInt(1, localeId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                String[] localeInfo = resultSet.getString(LOCALE_NAME_COLUMN).split(LOCALE_LINE_DELIMITER);
                return new Locale(localeInfo[LANGUAGE], localeInfo[COUNTRY]);
            } else {
                throw new DAOException("Can't find locale name by locale id in database.");
            }
        } catch (SQLException e) {
            throw new DAOException("Problems with finding locale id by locale name in database.", e);
        }
    }

    public int findLocaleIdByLocale(Locale locale) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_LOCALE_ID_BY_LOCALE_NAME)) {
            statement.setString(1, locale.toString());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(LOCALE_ID_COLUMN);
            } else {
                throw new DAOException("Can't find locale id by locale name in database.");
            }
        } catch (SQLException e) {
            throw new DAOException("Problems with finding locale id by locale name in database.", e);
        }
    }


}
