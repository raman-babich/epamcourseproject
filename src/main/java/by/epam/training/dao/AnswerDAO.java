package by.epam.training.dao;

import by.epam.training.entity.Answer;
import by.epam.training.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class AnswerDAO extends AbstractDAO {

    private static final String SQL_ADD_ANSWER = "INSERT INTO `answers`(`questions_question_id`, `answer_text`) VALUES (?, ?)";
    private static final String SQL_UPDATE_TEXT_BY_ID = "UPDATE `answers` SET `answer_text` = ? WHERE `answer_id` = ?";
    private static final String SQL_FIND_ANSWERS_BY_QUESTION_ID = "SELECT `answer_id`, `questions_question_id`, `answer_text` FROM `answers` WHERE `questions_question_id` = ?";
    private static final String SQL_ADD_ACCOUNTS_M2M_ANSWERS_RAW = "INSERT INTO `accounts_m2m_answers`(`answers_answer_id`, `accounts_account_id`, `answer_date_time`) VALUES (?, ?, NOW())";
    private static final String SQL_FIND_ANSWERS_BY_ACCOUNT_ID = "SELECT `answer_id`, `answer_text` FROM " +
            "(SELECT `answer_id`, `answer_text` FROM `questions` JOIN `answers` ON `questions`.`question_id` = `answers`.`questions_question_id` WHERE question_id = ?) as `ans` " +
            "JOIN `accounts_m2m_answers` ON `ans`.`answer_id` = `accounts_m2m_answers`.`answers_answer_id` WHERE accounts_account_id = ?";

    private static final String ANSWER_ID_COLUMN = "answer_id";
    private static final String QUESTION_ID_COLUMN = "questions_question_id";
    private static final String ANSWER_TEXT_COLUMN = "answer_text";

    public AnswerDAO(Connection connection) {
        super(connection);
    }

    public Answer addAnswer(int questionId, String text) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_ANSWER, Statement.RETURN_GENERATED_KEYS)) {
            Answer answer = new Answer();
            statement.setInt(1, questionId);
            statement.setString(2, text);
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                answer = new Answer();
                answer.setAnswerId(resultSet.getInt(1));
                answer.setQuestionId(questionId);
                answer.setText(text);
            }
            return answer;
        } catch (SQLException e) {
            throw new DAOException("Problems with adding answer to database.", e);
        }
    }

    public void commitAnswer(int answerId, int accountId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_ADD_ACCOUNTS_M2M_ANSWERS_RAW)) {
            statement.setInt(1, answerId);
            statement.setInt(2, accountId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with adding answer to database.", e);
        }
    }

    public void updateTextById(int answerId, String text) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TEXT_BY_ID)) {
            statement.setString(1, text);
            statement.setInt(2, answerId);
            statement.execute();
        } catch (SQLException e) {
            throw new DAOException("Problems with updating answer text by id to database.", e);
        }
    }

    public List<Answer> findAnswersByQuestionId(int questionId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ANSWERS_BY_QUESTION_ID)) {
            List<Answer> answers = new ArrayList<>();
            statement.setInt(1, questionId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                answers.add(initAnswer(resultSet, questionId));
            }
            return answers;
        } catch (SQLException e) {
            throw new DAOException("Problems with finding answer by question id in database.", e);
        }
    }

    public List<Answer> findAnswersByAccountId(int questionId, int accountId) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(SQL_FIND_ANSWERS_BY_ACCOUNT_ID)) {
            List<Answer> answers = new ArrayList<>();
            statement.setInt(1, questionId);
            statement.setInt(2, accountId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                answers.add(initAnswer(resultSet, questionId));
            }
            return answers;
        } catch (SQLException e) {
            throw new DAOException("Problems with finding answer by account id and question id in database.", e);
        }
    }

    private Answer initAnswer(ResultSet resultSet, int questionId) throws SQLException {
        Answer answer = new Answer();
        answer.setAnswerId(resultSet.getInt(ANSWER_ID_COLUMN));
        answer.setQuestionId(questionId);
        answer.setText(resultSet.getString(ANSWER_TEXT_COLUMN));
        return answer;
    }

}
