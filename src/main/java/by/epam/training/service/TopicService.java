package by.epam.training.service;

import by.epam.training.ajax.BiTuple;
import by.epam.training.command.CommandConstants;
import by.epam.training.dao.TopicDAO;
import by.epam.training.database.ConnectionPool;
import by.epam.training.entity.Topic;
import by.epam.training.exception.DAOException;
import by.epam.training.exception.ServiceException;
import by.epam.training.manager.MessageManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

/**
 *
 */
public class TopicService {

    private static final int DEFAULT_TOPIC_ID = 1;

    public enum Outcome {
        VALID, INVALID, NAME_NOT_UNIQUE
    }

    public BiTuple<Outcome, Topic> addTopic(String topicName) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            TopicDAO topicDAO = new TopicDAO(connection);
            BiTuple<TopicService.Outcome, Topic> result = new BiTuple<>(Outcome.NAME_NOT_UNIQUE, null);
            if (topicDAO.checkTopicNameUniqueness(topicName)) {
                result.setLeft(Outcome.VALID);
                result.setRight(topicDAO.addTopic(topicName));
            }
            connection.commit();
            return result;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with topic add operation.", e);
        }
    }

    public List<Topic> loadTopics() throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            TopicDAO topicDAO = new TopicDAO(connection);
            List<Topic> topics = topicDAO.listAllTopics();
            connection.commit();
            return topics;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with load topics operation.", e);
        }
    }

    public Outcome removeTopic(int id) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            Outcome outcome = Outcome.INVALID;
            if (id != DEFAULT_TOPIC_ID) {
                TopicDAO topicDAO = new TopicDAO(connection);
                topicDAO.removeTopicById(id);
                outcome = Outcome.VALID;
            }
            connection.commit();
            return outcome;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with topic remove operation.", e);
        }
    }

    public Outcome updateTopic(int id, String name) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            Outcome result = Outcome.INVALID;
            TopicDAO topicDAO = new TopicDAO(connection);
            if (id != DEFAULT_TOPIC_ID) {
                topicDAO.updateTopicNameById(id, name);
                result = Outcome.VALID;
            } else if (!topicDAO.checkTopicNameUniqueness(name)) {
                result = Outcome.NAME_NOT_UNIQUE;
            }
            connection.commit();
            return result;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with update topic operation.", e);
        }
    }

    public static String processOutcome(TopicService.Outcome outcome, Locale locale) {
        String result = "";
        switch (outcome) {
            case VALID:
                result = MessageManager.getProperty(CommandConstants.MESSAGE_TOPIC_SUCCESSFULLY_ADDED, locale);
                break;
            case INVALID:
                result = MessageManager.getProperty(CommandConstants.MESSAGE_TOPIC_DEFAULT_ID, locale);
                break;
            case NAME_NOT_UNIQUE:
                result = MessageManager.getProperty(CommandConstants.MESSAGE_TOPIC_NAME_IS_NOT_UNIQUE, locale);
                break;
        }
        return result;
    }

}
