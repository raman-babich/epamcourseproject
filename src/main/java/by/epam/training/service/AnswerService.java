package by.epam.training.service;

import by.epam.training.dao.AnswerDAO;
import by.epam.training.database.ConnectionPool;
import by.epam.training.entity.Answer;
import by.epam.training.exception.DAOException;
import by.epam.training.exception.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public class AnswerService {

    public Answer addAnswer(int questionId, String text) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            Answer answer = answerDAO.addAnswer(questionId, text);
            connection.commit();
            return answer;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with adding answer information.", e);
        }
    }

    public void updateText(int answerId, String text) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            answerDAO.updateTextById(answerId, text);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating answer text.", e);
        }
    }

    public List<Answer> loadAnswers(int questionId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            List<Answer> answers = answerDAO.findAnswersByQuestionId(questionId);
            connection.commit();
            return answers;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading answers.", e);
        }
    }

    public void commitUserAnswer(List<Integer> answerId, int accountId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            for (Integer id: answerId) {
                answerDAO.commitAnswer(id, accountId);
            }
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with commiting answers.", e);
        }
    }

}
