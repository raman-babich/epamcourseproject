package by.epam.training.service;

import by.epam.training.dao.QuestionDAO;
import by.epam.training.database.ConnectionPool;
import by.epam.training.entity.Question;
import by.epam.training.exception.DAOException;
import by.epam.training.exception.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 */
public class QuestionService {

    public Question addQuestion(int surveyId, int topicId, String text, boolean answerType) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            QuestionDAO questionDAO = new QuestionDAO(connection);
            Question question = questionDAO.addQuestion(surveyId, topicId, text, answerType);
            connection.commit();
            return question;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with adding question information.", e);
        }
    }

    public void updateSurveyId(int questionId, int surveyId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            QuestionDAO questionDAO = new QuestionDAO(connection);
            questionDAO.updateSurveyIdById(questionId, surveyId);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating question survey id.", e);
        }
    }

    public void updateTopicId(int questionId, int topicId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            QuestionDAO questionDAO = new QuestionDAO(connection);
            questionDAO.updateTopicIdById(questionId, topicId);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating question topic id.", e);
        }
    }

    public void updateText(int questionId, String text) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            QuestionDAO questionDAO = new QuestionDAO(connection);
            questionDAO.updateTextById(questionId, text);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating question text.", e);
        }
    }

    public void updateAnswerType(int questionId, boolean answerType) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            QuestionDAO questionDAO = new QuestionDAO(connection);
            questionDAO.updateAnswerTypeById(questionId, answerType);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating question type.", e);
        }
    }

}
