package by.epam.training.service;

import by.epam.training.dao.AccountDAO;
import by.epam.training.dao.AppLocaleDAO;
import by.epam.training.database.ConnectionPool;
import by.epam.training.entity.Account;
import by.epam.training.exception.DAOException;
import by.epam.training.exception.ServiceException;
import by.epam.training.validator.AccountValidator;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

/**
 *
 */
public class AccountService {

    public enum Outcome {
        VALID, INVALID, INVALID_LOGIN, INVALID_EMAIL, INVALID_PASSWORD, PASSWORDS_NOT_EQUALS, EMAIL_NOT_UNIQUE, LOGIN_NOT_UNIQUE, INCORRECT_PASSWORD, INVALID_NEW_PASSWORD
    }

    public Outcome signUp(String firstName, String secondName, String login, String email, String password, String repeatedPassword, Locale locale) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            Outcome outcome;
            AccountDAO accountDAO = new AccountDAO(connection);
            AppLocaleDAO appLocaleDAO = new AppLocaleDAO(connection);
            if (!AccountValidator.validateLogin(login)) {
                outcome = Outcome.INVALID_LOGIN;
            } else if (!AccountValidator.validateEmail(email)) {
                outcome = Outcome.INVALID_EMAIL;
            } else if (!AccountValidator.validatePassword(password)) {
                outcome = Outcome.INVALID_PASSWORD;
            } else if (!password.equals(repeatedPassword)) {
                outcome = Outcome.PASSWORDS_NOT_EQUALS;
            } else if (!accountDAO.checkEmailUniqueness(email)) {
                outcome = Outcome.EMAIL_NOT_UNIQUE;
            } else if (!accountDAO.checkLoginUniqueness(login)) {
                outcome = Outcome.LOGIN_NOT_UNIQUE;
            } else {
                accountDAO.addAccount(firstName, secondName, login, email, password, appLocaleDAO.findLocaleIdByLocale(locale));
                outcome = Outcome.VALID;
            }
            connection.commit();
            return outcome;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with signUp operation.", e);
        }
    }

    public boolean signIn(String emailOrLogin, String password) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            boolean result = false;
            AccountDAO accountDAO = new AccountDAO(connection);
            if ((AccountValidator.validateEmail(emailOrLogin) ||
                    AccountValidator.validateLogin(emailOrLogin)) &&
                    AccountValidator.validatePassword(password)) {
                result = accountDAO.checkPassword(emailOrLogin, password);
            }
            connection.commit();
            return result;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with signIn operation.", e);
        }
    }

    public boolean isAdmin(String emailOrLogin) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            boolean result = accountDAO.checkAdminPermission(emailOrLogin);
            connection.commit();
            return result;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with checking admin permission.", e);
        }
    }

    public Account loadAccount(String emailOrLogin) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            Account account = accountDAO.findAccountByEmailOrLogin(emailOrLogin);
            connection.commit();
            return account;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading account information.", e);
        }
    }

    public List<Account> loadAccounts() throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            List<Account> accounts = accountDAO.listAllAccounts();
            connection.commit();
            return accounts;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading account information.", e);
        }
    }

    public int changeLocale(int accountId, Locale locale) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            AppLocaleDAO appLocaleDAO = new AppLocaleDAO(connection);
            int localeId = appLocaleDAO.findLocaleIdByLocale(locale);
            accountDAO.updateLocaleByAccountId(accountId, localeId);
            connection.commit();
            return localeId;//TODO: what about javadoc here?
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with changing locale.", e);
        }
    }

    public void updateFirstName(int accountId, String firstName) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            accountDAO.updateFirstNameByAccountId(accountId, firstName);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating account first name.", e);
        }
    }

    public void updateSecondName(int accountId, String secondName) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            accountDAO.updateSecondNameByAccountId(accountId, secondName);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating account second name.", e);
        }
    }

    public Outcome updateLogin(int accountId, String login) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            Outcome outcome;
            AccountDAO accountDAO = new AccountDAO(connection);
            if (!AccountValidator.validateLogin(login)) {
                outcome = Outcome.INVALID_LOGIN;
            } else if (!accountDAO.checkLoginUniqueness(login)) {
                outcome = Outcome.LOGIN_NOT_UNIQUE;
            } else {
                accountDAO.updateLoginByAccountId(accountId, login);
                outcome = Outcome.VALID;
            }
            accountDAO.updateLoginByAccountId(accountId, login);
            connection.commit();
            return outcome;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating account login.", e);
        }

    }

    public Outcome updateEmail(int accountId, String email) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            Outcome outcome;
            AccountDAO accountDAO = new AccountDAO(connection);
            if (!AccountValidator.validateEmail(email)) {
                outcome = Outcome.INVALID_EMAIL;
            } else if (!accountDAO.checkEmailUniqueness(email)) {
                outcome = Outcome.EMAIL_NOT_UNIQUE;
            } else {
                accountDAO.updateEmailByAccountId(accountId, email);
                outcome = Outcome.VALID;
            }
            accountDAO.updateEmailByAccountId(accountId, email);
            connection.commit();
            return outcome;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating account email.", e);
        }
    }

    public void updateAvatar(int accountId, byte[] avatar) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AccountDAO accountDAO = new AccountDAO(connection);
            accountDAO.updateAvatarByAccountId(accountId, avatar);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating account avatar.", e);
        }
    }

    public Outcome updatePassword(int accountId, String oldPassword, String newPassword, String repeatedNewPassword) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            Outcome outcome;
            AccountDAO accountDAO = new AccountDAO(connection);
            if (!AccountValidator.validatePassword(oldPassword)) {
                outcome = Outcome.INVALID_PASSWORD;
            } else if (!accountDAO.checkPassword(accountId, oldPassword)) {
                outcome = Outcome.INCORRECT_PASSWORD;
            } else if (!AccountValidator.validatePassword(newPassword)) {
                outcome = Outcome.INVALID_NEW_PASSWORD;
            } else if (!newPassword.equals(repeatedNewPassword)) {
                outcome = Outcome.PASSWORDS_NOT_EQUALS;
            } else {
                accountDAO.updatePasswordByAccountId(accountId, newPassword);
                outcome = Outcome.VALID;
            }
            connection.commit();
            return outcome;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating account password.", e);
        }
    }

}
