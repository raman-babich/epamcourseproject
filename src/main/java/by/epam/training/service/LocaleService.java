package by.epam.training.service;

import by.epam.training.dao.AppLocaleDAO;
import by.epam.training.database.ConnectionPool;
import by.epam.training.exception.DAOException;
import by.epam.training.exception.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

/**
 *
 */
public class LocaleService {

    public Locale findLocale(int localeId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            AppLocaleDAO appLocaleDAO = new AppLocaleDAO(connection);
            Locale locale = appLocaleDAO.findLocaleByLocaleId(localeId);
            connection.commit();
            return locale;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with finding locale.", e);
        }
    }

}
