package by.epam.training.service;

import by.epam.training.ajax.BiTuple;
import by.epam.training.dao.AnswerDAO;
import by.epam.training.dao.QuestionDAO;
import by.epam.training.dao.SurveyDAO;
import by.epam.training.database.ConnectionPool;
import by.epam.training.entity.Answer;
import by.epam.training.entity.Question;
import by.epam.training.entity.Survey;
import by.epam.training.exception.DAOException;
import by.epam.training.exception.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SurveyService {

    public Survey addSurvey(String title, String description, byte[] image,  boolean accessible) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            connection.commit();
            return surveyDAO.addSurvey(title, description, image, accessible);
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading surveys information.", e);
        }
    }

    public Survey addSurvey(String title, String description, byte[] image, boolean accessible, List<BiTuple<Question, List<Answer>>> surveyQuestions) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            QuestionDAO questionDAO = new QuestionDAO(connection);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            Survey survey = surveyDAO.addSurvey(title, description, image, accessible);
            int questionAmount = surveyQuestions.size();
            for (int i = 0; i < questionAmount; ++i) {
                Question question = surveyQuestions.get(i).getLeft();
                question = questionDAO.addQuestion(survey.getSurveyId(), question.getTopicId(), question.getText(), question.isAnswerType());
                surveyQuestions.get(i).setLeft(question);
                int answerAmount = surveyQuestions.get(i).getRight().size();
                for (int j = 0; j < answerAmount; ++j) {
                    Answer answer = surveyQuestions.get(i).getRight().get(j);
                    answer = answerDAO.addAnswer(question.getQuestionId(), answer.getText());
                    surveyQuestions.get(i).getRight().set(j, answer);
                }
            }
            connection.commit();
            return survey;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading surveys information.", e);
        }
    }

    public boolean checkIfPassedByAccountId(int surveyId, int accountId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            boolean result = surveyDAO.checkIfPassedByAccount(surveyId, accountId);
            connection.commit();
            return result;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with checking pass survey status of account.", e);
        }
    }

    public void updateTitle(int id, String title) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            surveyDAO.updateTitleById(id, title);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating survey title.", e);
        }
    }

    public void updateDescription(int id, String description) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            surveyDAO.updateDescriptionById(id, description);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating survey description.", e);
        }
    }

    public void updateImage(int id, byte[] image) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            surveyDAO.updateImageById(id, image);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating survey image.", e);
        }
    }

    public void updateAccessible(int id, boolean accessible) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            surveyDAO.updateAccessibleById(id, accessible);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating survey accessible.", e);
        }
    }

    public void updateAvailable(int id, boolean available) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            surveyDAO.updateAvailableById(id, available);
            connection.commit();
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with updating survey available.", e);
        }
    }

    public List<Survey> loadAccessibleSurveys(boolean accessible) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            List<Survey> list = surveyDAO.findSurveysByAccessible(accessible);
            connection.commit();
            return list;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading surveys information.", e);
        }
    }

    public List<Survey> loadAvailableSurveys(boolean available) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            List<Survey> list = surveyDAO.findSurveysByAvailable(available);
            connection.commit();
            return list;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading surveys information.", e);
        }
    }

    public Survey loadSurvey(int surveyId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            SurveyDAO surveyDAO = new SurveyDAO(connection);
            Survey survey = surveyDAO.findSurveyById(surveyId);
            connection.commit();
            return survey;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading surveys information.", e);
        }
    }

    public List<BiTuple<Question, List<Answer>>> loadSurveyQuestions(int surveyId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            List<BiTuple<Question, List<Answer>>> surveyQuestions = new ArrayList<>();
            QuestionDAO questionDAO = new QuestionDAO(connection);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            List<Question> questions = questionDAO.findQuestionsBySurveyId(surveyId);
            for (Question question : questions) {
                surveyQuestions.add(new BiTuple<>(question, answerDAO.findAnswersByQuestionId(question.getQuestionId())));
            }
            connection.commit();
            return surveyQuestions;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading full survey information.", e);
        }
    }

    public List<BiTuple<Question, List<Answer>>> loadSurveyResults(int surveyId, int accountId) throws ServiceException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            connection.setAutoCommit(false);
            List<BiTuple<Question, List<Answer>>> surveyResults = new ArrayList<>();
            QuestionDAO questionDAO = new QuestionDAO(connection);
            AnswerDAO answerDAO = new AnswerDAO(connection);
            List<Question> questions = questionDAO.findQuestionsBySurveyId(surveyId);
            for (Question question : questions) {
                surveyResults.add(new BiTuple<>(question, answerDAO.findAnswersByAccountId(question.getQuestionId(), accountId)));
            }
            connection.commit();
            return surveyResults;
        } catch (SQLException | DAOException e) {
            throw new ServiceException("Problems with loading survey results information.", e);
        }
    }

}
