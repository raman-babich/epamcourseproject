package by.epam.training.ajax;

/**
 *
 */
public enum AJAXState {
    OK, LOCATION_REDIRECT, LOCATION_GO, HANDLE, ERROR
}
