package by.epam.training.filter;

import by.epam.training.ajax.BiTuple;
import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.entity.Answer;
import by.epam.training.entity.Question;
import by.epam.training.entity.Survey;
import by.epam.training.exception.ServiceException;
import by.epam.training.service.SurveyService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@WebFilter(filterName = "AccountStatisticLoadFilter", urlPatterns = {"/jsp/admin/account_statistic.jsp"}, dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST})
public class AccountStatisticLoadFilter implements Filter {

    private static final String ACCOUNT_ID_PARAM = "id";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        SurveyService surveyService = new SurveyService();
        boolean skip = false;
        try {
            int accountId = Integer.parseInt(request.getParameter(ACCOUNT_ID_PARAM));
            List<Survey> accessibleSurveys = surveyService.loadAccessibleSurveys(true);
            request.getSession().setAttribute(ControllerConstants.ACCESSIBLE_SURVEYS_KEY, accessibleSurveys);
            List<BiTuple<Survey, List<BiTuple<Question, List<Answer>>>>> statistic = new ArrayList<>();
            int accessibleSurveyAmount = accessibleSurveys.size();
            for (int i = 0; i < accessibleSurveyAmount; ++i) {
                int surveyId = accessibleSurveys.get(i).getSurveyId();
                if (surveyService.checkIfPassedByAccountId(surveyId, accountId)) {
                    statistic.add(new BiTuple<>(surveyService.loadSurvey(surveyId), surveyService.loadSurveyResults(surveyId, accountId)));
                }
            }
            request.setAttribute(ControllerConstants.ACCOUNT_STATISTIC_KEY, statistic);
        } catch (ServiceException e) {
            AbstractHttpServletCommand.handleDBError(e, request, response);
            skip = true;
        }  catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            skip = true;
        }
        if (!skip) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
