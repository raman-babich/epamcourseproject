package by.epam.training.filter;

import by.epam.training.command.CommandManager;
import by.epam.training.command.IHttpServletCommand;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.controller.MainServletConfig;
import by.epam.training.entity.Visitor;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 *
 */
@WebFilter(filterName = "MainServletFilter", servletNames = {"MainServlet"}, dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class MainServletFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        MainServletConfig servletConfig = initConfig(request);
        Visitor visitor = (Visitor) request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        Map<String, IHttpServletCommand> commands = CommandManager.getCommands(visitor.getRole());
        if (!commands.containsKey(servletConfig.getCommand())) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private MainServletConfig initConfig(HttpServletRequest req) {
        MainServletConfig servletConfig = (MainServletConfig)req.getSession().getAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY);
        if (servletConfig == null) {
            servletConfig = new MainServletConfig();
            req.getSession().setAttribute(ControllerConstants.MAIN_SERVLET_CONFIG_KEY, servletConfig);
        }
        if (ControllerConstants.AJAX_HEADER_VALUE.equals(req.getHeader(ControllerConstants.X_REQUESTED_WITH))) {
            servletConfig.setState(MainServletConfig.State.AJAX);
        } else {
            servletConfig.setState(MainServletConfig.State.FORWARD);
        }
        servletConfig.setCommand(req.getParameter(ControllerConstants.COMMAND_PARAM));
        return servletConfig;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
