package by.epam.training.filter;

import by.epam.training.command.CommandConstants;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.entity.Visitor;
import by.epam.training.manager.ConfigManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 *
 */
@WebFilter(filterName = "VisitorFilter", urlPatterns = {"/*"})
public class VisitorFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        initUserState(request);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void initUserState(HttpServletRequest req) {
        Visitor visitor = (Visitor)req.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        if (visitor == null) {
            visitor = new Visitor();
            visitor.setRole(Visitor.Role.GUEST);
            visitor.setLocale(ControllerConstants.DEFAULT_LOCALE);
            visitor.setCurrentPage(ConfigManager.getProperty(CommandConstants.PATH_PAGE_INDEX));
            req.getSession().setAttribute(ControllerConstants.VISITOR_KEY, visitor);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
