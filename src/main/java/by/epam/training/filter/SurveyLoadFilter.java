package by.epam.training.filter;

import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.entity.Account;
import by.epam.training.entity.Survey;
import by.epam.training.entity.Visitor;
import by.epam.training.exception.ServiceException;
import by.epam.training.service.SurveyService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@WebFilter(filterName = "SurveyLoadFilter", urlPatterns = {"/jsp/user/main.jsp"}, dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST})
public class SurveyLoadFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Visitor visitor = (Visitor) request.getSession().getAttribute(ControllerConstants.VISITOR_KEY);
        SurveyService surveyService = new SurveyService();
        boolean skip = false;
        try {
            List<Survey> accessibleSurveys;
            List<Survey> passedSurvey = new ArrayList<>();
            List<Survey> notPassedSurvey = new ArrayList<>();
            Account account = (Account) request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
            accessibleSurveys = surveyService.loadAccessibleSurveys(true);
            for (Survey survey : accessibleSurveys) {
                if (surveyService.checkIfPassedByAccountId(survey.getSurveyId(), account.getAccountId())) {
                    passedSurvey.add(survey);
                } else {
                    notPassedSurvey.add(survey);
                }
            }
            request.getSession().setAttribute(ControllerConstants.PASSED_SURVEYS_KEY, passedSurvey);
            request.getSession().setAttribute(ControllerConstants.NOT_PASSED_SURVEYS_KEY, notPassedSurvey);
            if (visitor.getRole() == Visitor.Role.ADMIN) {
                request.getSession().setAttribute(ControllerConstants.ACCESSIBLE_SURVEYS_KEY, accessibleSurveys);
                List<Survey> notAccessibleSurveys = surveyService.loadAccessibleSurveys(false);
                request.getSession().setAttribute(ControllerConstants.NOT_ACCESSIBLE_SURVEYS_KEY, notAccessibleSurveys);
            }
        } catch (ServiceException e) {
            AbstractHttpServletCommand.handleDBError(e, request, response);
            skip = true;
        }
        if (!skip) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
