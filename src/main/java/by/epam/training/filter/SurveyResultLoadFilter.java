package by.epam.training.filter;

import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.entity.Account;
import by.epam.training.exception.ServiceException;
import by.epam.training.service.SurveyService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 */
@WebFilter(filterName = "SurveyResultLoadFilter", urlPatterns = {"/jsp/user/survey_result.jsp"}, dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST})
public class SurveyResultLoadFilter implements Filter {

    private static final String SURVEY_ID_PARAM = "id";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        SurveyService surveyService = new SurveyService();
        boolean skip = false;
        try {
            int surveyId = Integer.parseInt(request.getParameter(SURVEY_ID_PARAM));
            Account account = (Account) request.getSession().getAttribute(ControllerConstants.ACCOUNT_KEY);
            if (surveyService.checkIfPassedByAccountId(surveyId, account.getAccountId())) {
                request.setAttribute(ControllerConstants.PASSED_SURVEY_KEY, surveyService.loadSurvey(surveyId));
                request.setAttribute(ControllerConstants.PASSED_SURVEY_RESULT_KEY, surveyService.loadSurveyResults(surveyId, account.getAccountId()));
            } else {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                skip = true;
            }
        } catch (ServiceException e) {
            AbstractHttpServletCommand.handleDBError(e, request, response);
            skip = true;
        }  catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            skip = true;
        }
        if (!skip) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
