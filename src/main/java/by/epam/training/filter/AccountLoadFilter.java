package by.epam.training.filter;

import by.epam.training.command.AbstractHttpServletCommand;
import by.epam.training.controller.ControllerConstants;
import by.epam.training.exception.ServiceException;
import by.epam.training.service.AccountService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 */
@WebFilter(filterName = "AccountLoadFilter", urlPatterns = {"/jsp/admin/account_list.jsp"}, dispatcherTypes = {DispatcherType.FORWARD, DispatcherType.REQUEST})
public class AccountLoadFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        AccountService accountService = new AccountService();
        boolean skip = false;
        try {
            request.setAttribute(ControllerConstants.ACCOUNTS_KEY, accountService.loadAccounts());
        } catch (ServiceException e) {
            AbstractHttpServletCommand.handleDBError(e, request, response);
            skip = true;
        }
        if (!skip) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
