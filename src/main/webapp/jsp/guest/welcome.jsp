<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="srv" uri="http://epam.com/surveys/jsp/surveys" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><fmt:message key="label.main.title"/></title>

    <!-- Bootstrap -->
    <link href="${context}/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@include file="../../WEB-INF/jspf/header.jsp"%>
<main class="container">
    <div class="modal fade sign-up-modal-lg" tabindex="-1" role="dialog" aria-labelledby="SignUp">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title"><fmt:message key="label.sign.up"/></h3>
                </div>
                <div class="modal-body">
                    <form id="signUpForm" action="${context}/main" method="post">
                        <div class="form-group-lg">
                            <label for="signUpFirstName"><fmt:message key="label.first.name"/></label>
                            <input id="signUpFirstName" class="form-control" type="text" name="firstName" placeholder="<fmt:message key="label.first.name"/>" value="${srv:nullCheck(firstName)}">
                        </div>
                        <div class="form-group-lg">
                            <label for="signUpSecondName"><fmt:message key="label.second.name"/></label>
                            <input id="signUpSecondName" class="form-control" type="text" name="secondName" placeholder="<fmt:message key="label.second.name"/>" value="${srv:nullCheck(secondName)}">
                        </div>
                        <div class="form-group-lg">
                            <label for="signUpLogin"><fmt:message key="label.login"/></label>
                            <input id="signUpLogin" class="form-control" type="text" name="login" placeholder="<fmt:message key="label.login"/>" value="${srv:nullCheck(login)}">
                        </div>
                        <div class="form-group-lg">
                            <label for="signUpEmail"><fmt:message key="label.email"/></label>
                            <input id="signUpEmail" class="form-control" type="text" name="email" placeholder="<fmt:message key="label.email"/>" value="${srv:nullCheck(email)}">
                        </div>
                        <div class="form-group-lg">
                            <label for="signUpPassword"><fmt:message key="label.password"/></label>
                            <input id="signUpPassword" class="form-control" type="password" name="signUpPassword" placeholder="<fmt:message key="label.password"/>" value="${srv:nullCheck(signUpPassword)}">
                        </div>
                        <div class="form-group-lg">
                            <label for="signUpRepeatedPassword"><fmt:message key="label.repeated.password"/></label>
                            <input id="signUpRepeatedPassword" class="form-control" type="password" name="repeatedSignUpPassword" placeholder="<fmt:message key="label.repeated.password"/>">
                        </div>
                        <input type="hidden" name="command" value="signUp">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="label.close"/></button>
                    <button type="submit" form="signUpForm" class="btn btn-primary"><fmt:message key="label.sign.up"/></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade sign-in-modal-lg" tabindex="-1" role="dialog" aria-labelledby="SignIn">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title"><fmt:message key="label.sign.in"/></h3>
                </div>
                <div class="modal-body">
                    <form id="signInForm" action="${context}/main" method="post">
                        <div class="form-group-lg">
                            <label for="signInEmailOrLogin"><fmt:message key="label.email.or.login"/></label>
                            <input id="signInEmailOrLogin" type="text" class="form-control" name="emailOrLogin" placeholder="<fmt:message key="label.email.or.login"/>" value="${srv:nullCheck(emailOrLogin)}">
                        </div>
                        <div class="form-group-lg">
                            <label for="signInPassword"><fmt:message key="label.password"/></label>
                            <input id="signInPassword" type="password" class="form-control" name="signInPassword" placeholder="<fmt:message key="label.password"/>" value="${srv:nullCheck(signInPassword)}">
                        </div>
                        <input type="hidden" name="command" value="signIn">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="label.close"/></button>
                    <button type="submit" form="signInForm" class="btn btn-primary"><fmt:message key="label.sign.in"/></button>
                </div>
            </div>
        </div>
    </div>
    <%@include file="../../WEB-INF/jspf/about_content.jsp"%>
</main>
<%@include file="../../WEB-INF/jspf/footer.jsp"%>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${context}/js/jquery-3.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${context}/js/jquery.form.min.js"></script>
<script src="${context}/js/sign_manage.js"></script>
<script src="${context}/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
