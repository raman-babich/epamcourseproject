<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="srv" uri="http://epam.com/surveys/jsp/surveys" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><fmt:message key="label.main.title"/></title>

    <!-- Bootstrap -->
    <link href="${context}/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@include file="../../WEB-INF/jspf/header.jsp"%>
<main class="container">
    <div id="addSurveyPanel" class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title text-center"><fmt:message key="label.add.survey"/></h2>
        </div>
        <div class="panel-body">
            <form id="addSurveyForm" action="${context}/main" method="post" enctype="multipart/form-data">
                <div class="form-group-lg">
                    <label for="surveyTitle"><fmt:message key="label.survey.title"/></label>
                    <input id="surveyTitle" type="text" required class="form-control" name="surveyTitle" placeholder="<fmt:message key="label.survey.title"/>">
                </div>
                <div class="form-group-lg">
                    <label for="surveyDescription"><fmt:message key="label.survey.description"/></label>
                    <textarea id="surveyDescription" required form="addSurveyForm" class="form-control" name="surveyDescription" placeholder="<fmt:message key="label.survey.description"/>"></textarea>
                </div>
                <div class="form-group-lg">
                    <label for="surveyImage"><fmt:message key="label.survey.image"/></label>
                    <input id="surveyImage" type="file" name="surveyImage">
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="surveyAccessible" value="accessible"><fmt:message key="label.is.accessible"/></label>
                </div>
                <div id="surveyQuestions" class="form-group-lg">
                    <label><fmt:message key="label.survey.questions"/></label>
                    <div id="surveyQuestion-0" class="form-group-lg">
                        <div class="text-center bg-info"><fmt:message key="label.survey.question"/></div>
                        <label for="surveyQuestionTopic-0"><fmt:message key="label.topic"/></label>
                        <select id="surveyQuestionTopic-0" class="form-inline" name="surveyQuestionTopic-0">
                            <c:forEach var="topic" items="${topics}">
                                <option value="${topic.topicId}">${topic.name}</option>
                            </c:forEach>
                        </select>
                        <textarea form="addSurveyForm" required class="form-control" name="surveyQuestionText-0" placeholder="<fmt:message key="label.survey.question"/>"></textarea>
                        <button id="addAnswer-0" type="button" class="btn btn-default"><fmt:message key="label.add.answer"/></button>
                        <button id="removeAnswer-0" type="button" class="btn btn-default"><fmt:message key="label.remove.answer"/></button>
                        <div id="questionAnswer-0" class="form-group-lg">
                            <input type="text" required class="form-control" name="questionAnswer-0-0" placeholder="<fmt:message key="label.answer"/>">
                            <input type="text" required class="form-control" name="questionAnswer-0-1" placeholder="<fmt:message key="label.answer"/>">
                        </div>
                        <input id="questionAnswerAmount-0" type="hidden" name="questionAnswerAmount-0" value="2">
                        <label class="radio-inline"><input type="radio" name="surveyQuestionAnswerType-0" value="radio" checked><fmt:message key="label.radio.buttons"/></label>
                        <label class="radio-inline"><input type="radio" name="surveyQuestionAnswerType-0" value="checkbox"><fmt:message key="label.checkboxes"/></label>
                    </div>
                </div>
                <input id="surveyQuestionAmount" type="hidden" name="surveyQuestionAmount" value="1">
                <input type="hidden" name="command" value="addSurvey">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <button id="removeQuestionButton" type="button" class="btn btn-default pull-left"><fmt:message key="label.survey.remove.question"/></button>
            <button id="addQuestionButton" type="button" class="btn btn-default pull-left"><fmt:message key="label.survey.add.question"/></button>
            <button type="submit" form="addSurveyForm" class="btn btn-primary pull-right"><fmt:message key="label.add.survey"/></button>
        </div>
    </div>
</main>
<%@include file="../../WEB-INF/jspf/footer.jsp"%>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${context}/js/jquery-3.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${context}/js/jquery.form.min.js"></script>
<script src="${context}/js/survey_manage.js"></script>
<script src="${context}/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
