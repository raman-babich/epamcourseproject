<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="srv" uri="http://epam.com/surveys/jsp/surveys" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><fmt:message key="label.main.title"/></title>

    <!-- Bootstrap -->
    <link href="${context}/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@include file="../../WEB-INF/jspf/header.jsp"%>
<main class="container">
    <div id="notAccessibleSurveys" class="clearfix">
        <div class="page-header text-center"><h2><fmt:message key="label.not.accessible.surveys"/></h2></div>
        <c:forEach var="survey" items="${notAccessibleSurveys}">
            <div class="panel panel-default col-lg-6">
                <div class="panel-heading">
                    <h2 class="panel-title text-center">${survey.title}</h2>
                </div>
                <div class="panel-body">
                    <div class="col-lg-6"><img width="100%" src="${context}/main?command=loadImage&src=survey&id=${survey.surveyId}" alt="Survey image"/></div>
                    <div class="col-lg-6">${survey.description}</div>
                    <label><fmt:message key="label.creating.time"/></label>
                    <div>${survey.creatingTime}</div>
                </div>
                <div class="panel-footer clearfix">
                    <form id="notAccessibleForm-${survey.surveyId}" action="${context}/main" method="post">
                        <input type="hidden" name="command" value="updateAccess">
                        <input type="hidden" name="value" value="true">
                        <input type="hidden" name="id" value="${survey.surveyId}">
                        <button type="submit" class="btn btn-primary pull-right"><fmt:message key="label.change.access"/></button>
                    </form>
                </div>
            </div>
        </c:forEach>
    </div>
    <div id="accessibleSurveys" class="clearfix">
        <div class="page-header text-center"><h2><fmt:message key="label.accessible.surveys"/></h2></div>
        <c:forEach var="survey" items="${accessibleSurveys}">
            <div class="panel panel-default col-lg-6">
                <div class="panel-heading">
                    <h2 class="panel-title text-center">${survey.title}</h2>
                </div>
                <div class="panel-body">
                    <div class="col-lg-6"><img width="100%" src="${context}/main?command=loadImage&src=survey&id=${survey.surveyId}" alt="Survey image"/></div>
                    <div class="col-lg-6">${survey.description}</div>
                    <label><fmt:message key="label.creating.time"/></label>
                    <div>${survey.creatingTime}</div>
                </div>
                <div class="panel-footer clearfix">
                    <form id="accessibleForm-${survey.surveyId}" action="${context}/main" method="post">
                        <input type="hidden" name="command" value="updateAccess">
                        <input type="hidden" name="value" value="false">
                        <input type="hidden" name="id" value="${survey.surveyId}">
                        <button type="submit" class="btn btn-primary pull-right"><fmt:message key="label.change.access"/></button>
                    </form>
                </div>
            </div>
        </c:forEach>
    </div>
</main>
<%@include file="../../WEB-INF/jspf/footer.jsp"%>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${context}/js/jquery-3.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${context}/js/jquery.form.min.js"></script>
<script src="${context}/js/survey_manage.js"></script>
<script src="${context}/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
