<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="srv" uri="http://epam.com/surveys/jsp/surveys" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><fmt:message key="label.main.title"/></title>

    <!-- Bootstrap -->
    <link href="${context}/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<main class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="text-center">${conductedSurvey.title}</h1>
        </div>
        <div class="panel-body">
            <form id="passSurveyForm" action="${context}/main" method="post">
                <c:forEach var="question" items="${conductedSurveyQuestions}" varStatus="i">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title text-center">${question.left.text}</h2>
                        </div>
                        <div class="panel-body">
                            <c:forEach var="answer" items="${question.right}" varStatus="j">
                                <c:choose>
                                    <c:when test="${question.left.answerType}">
                                        <div class="radio">
                                            <label><input type="radio" name="answer-${question.left.questionId}" value="${answer.answerId}">${answer.text}</label>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="answer-${question.left.questionId}-${j.index}" value="${answer.answerId}">${answer.text}</label>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </div>
                    </div>
                </c:forEach>
                <input type="hidden" name="command" value="passSurvey">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <button type="button" onclick="location.href='${context}/jsp/user/main.jsp?'" class="btn btn-primary pull-left"><fmt:message key="label.error.to.main"/></button>
            <button type="submit" form="passSurveyForm" class="btn btn-primary pull-right"><fmt:message key="label.submit"/></button>
        </div>
    </div>
</main>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${context}/js/jquery-3.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${context}/js/jquery.form.min.js"></script>
<script src="${context}/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
