<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="srv" uri="http://epam.com/surveys/jsp/surveys" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setLocale value="${visitor.locale}" scope="session"/>
<fmt:setBundle basename="properties.content"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><fmt:message key="label.main.title"/></title>

    <!-- Bootstrap -->
    <link href="${context}/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@include file="../../WEB-INF/jspf/header.jsp"%>
<main class="container">
    <div id="changeNamePanel" class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title text-center"><fmt:message key="label.first.name.and.second.name"/></h2>
        </div>
        <div class="panel-body">
            <form id="changeNameForm" action="${context}/main" method="post">
                <div class="form-group-lg">
                    <label for="changeFirstName"><fmt:message key="label.first.name"/></label>
                    <input id="changeFirstName" type="text" class="form-control" name="firstName" placeholder="<fmt:message key="label.first.name"/>" value="${srv:nullCheck(account.firstName)}">
                </div>
                <div class="form-group-lg">
                    <label for="changeSecondName"><fmt:message key="label.second.name"/></label>
                    <input id="changeSecondName" type="text" class="form-control" name="secondName" placeholder="<fmt:message key="label.second.name"/>" value="${srv:nullCheck(account.secondName)}">
                </div>
                <input type="hidden" name="command" value="changeName">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <button type="submit" form="changeNameForm" class="btn btn-primary pull-right"><fmt:message key="label.submit"/></button>
        </div>
    </div>
    <div id="changeAvatarPanel" class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title text-center"><fmt:message key="label.avatar"/></h2>
        </div>
        <div class="panel-body">
            <form id="changeAvatarForm" action="${context}/main" method="post" enctype="multipart/form-data">
                <div class="form-group-lg">
                    <label for="changeAvatar"><fmt:message key="label.avatar"/></label>
                    <input id="changeAvatar" type="file" name="avatar">
                </div>
                <input type="hidden" name="command" value="changeAvatar">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <form id="resetAvatarForm" action="${context}/main" method="post">
                <button type="submit" class="btn btn-default pull-left"  name="command" value="resetAvatar"><fmt:message key="label.reset"/></button>
            </form>
            <button type="submit" form="changeAvatarForm" class="btn btn-primary pull-right"><fmt:message key="label.submit"/></button>
        </div>
    </div>
    <div id="changeLoginPanel" class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title text-center"><fmt:message key="label.login"/></h2>
        </div>
        <div class="panel-body">
            <form id="changeLoginForm" action="${context}/main" method="post">
                <div class="form-group-lg">
                    <label for="changeLogin"><fmt:message key="label.login"/></label>
                    <input id="changeLogin" pattern="^\p{L}(\p{L}|\p{N}|[_])*$" type="text" class="form-control" name="login" placeholder="<fmt:message key="label.login"/>" value="${srv:nullCheck(account.login)}">
                </div>
                <input type="hidden" name="command" value="changeLogin">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <button type="submit" form="changeLoginForm" class="btn btn-primary pull-right"><fmt:message key="label.submit"/></button>
        </div>
    </div>
    <div id="changeEmailPanel" class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title text-center"><fmt:message key="label.email"/></h2>
        </div>
        <div class="panel-body">
            <form id="changeEmailForm" action="${context}/main" method="post">
                <div class="form-group-lg">
                    <label for="changeEmail"><fmt:message key="label.email"/></label>
                    <input id="changeEmail" pattern="^.+@.+[.].+$" type="text" class="form-control" name="email" placeholder="<fmt:message key="label.email"/>" value="${srv:nullCheck(account.email)}">
                </div>
                <input type="hidden" name="command" value="changeEmail">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <button type="submit" form="changeEmailForm" class="btn btn-primary pull-right"><fmt:message key="label.submit"/></button>
        </div>
    </div>
    <div id="changePasswordPanel" class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title text-center"><fmt:message key="label.password"/></h2>
        </div>
        <div class="panel-body">
            <form id="changePasswordForm" action="${context}/main" method="post">
                <div class="form-group-lg">
                    <label for="changeOldPassword"><fmt:message key="label.old.password"/></label>
                    <input id="changeOldPassword" pattern="^.*(?=.{6,})(?=.*(\p{Ll})+)(?=.*(\p{Lu})+)(?=.*(\p{N})+).*$" type="password" class="form-control" name="oldPassword" placeholder="<fmt:message key="label.old.password"/>">
                </div>
                <div class="form-group-lg">
                    <label for="changeNewPassword"><fmt:message key="label.new.password"/></label>
                    <input id="changeNewPassword" pattern="^.*(?=.{6,})(?=.*(\p{Ll})+)(?=.*(\p{Lu})+)(?=.*(\p{N})+).*$" type="password" class="form-control" name="newPassword" placeholder="<fmt:message key="label.new.password"/>">
                </div>
                <div class="form-group-lg">
                    <label for="changeNewRepeatedPassword"><fmt:message key="label.repeat.new.password"/></label>
                    <input id="changeNewRepeatedPassword" pattern="^.*(?=.{6,})(?=.*(\p{Ll})+)(?=.*(\p{Lu})+)(?=.*(\p{N})+).*$" type="password" class="form-control" name="repeatedNewPassword" placeholder="<fmt:message key="label.repeat.new.password"/>">
                </div>
                <input type="hidden" name="command" value="changePassword">
            </form>
        </div>
        <div class="panel-footer clearfix">
            <button type="submit" form="changePasswordForm" class="btn btn-primary pull-right"><fmt:message key="label.submit"/></button>
        </div>
    </div>
</main>
<%@include file="../../WEB-INF/jspf/footer.jsp"%>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${context}/js/jquery-3.1.1.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${context}/js/jquery.form.min.js"></script>
<script src="${context}/js/profile_manage.js"></script>
<script src="${context}/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
