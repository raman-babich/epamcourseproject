$(document).ready(function() {

    jQuery.fn.preventDoubleSubmission = function() {
        $(this).on('submit',function(e){
            var $form = $(this);
            if ($form.data('submitted')) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
            }
        });
        return this;
    };

    var signUpForm = '#signUpForm';
    var signInForm = '#signInForm';
    var $signUpForm = $(signUpForm);
    var $signInForm = $(signInForm);

    $signUpForm.preventDoubleSubmission();

    $signUpForm.ajaxForm(function(response) {
        var respSelector = '<div class="response"></div>';
        var formSelector = '#signUpForm .form-group-lg';
        var $respTag = $(respSelector);
        var $formGroups = $(formSelector);
        var $inputs = $('#signUpForm input[type="text"], #signUpForm input[type="password"]');
        $formGroups.removeClass('has-warning');
        $formGroups.removeClass('has-error');
        $('.response').remove();
        switch(response.left) {
            case 'HANDLE':
                $.each(response.right.invalidParams, function (index, value) {
                    var inputSelector = 'input[name=' + value + ']';
                    var $input = $(inputSelector);
                    $input.parent().addClass('has-warning');
                    $input.val('');
                    $respTag.text(response.right.message);
                    $respTag.addClass('alert alert-warning');
                    $respTag.appendTo($input.parent())
                });
                $signUpForm.data('submitted', false);
                break;
            case 'OK':
                $respTag.text(response.right.message);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($signUpForm);
                $inputs.val('');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($signUpForm);
                $signUpForm.data('submitted', false);
                break;
        }
    });

    $signInForm.ajaxForm(function(response) {
        var respSelector = '<p class="response"></p>';
        var $respTag = $(respSelector);
        var $inputs = $('#signInEmailOrLogin, #signInPassword');
        $('.response').remove();
        switch(response.left) {
            case 'HANDLE':
                $respTag.text(response.right);
                $respTag.appendTo($signInForm);
                $inputs.val('');
                $inputs.parent().addClass('has-warning');
                $respTag.addClass('alert alert-warning');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'LOCATION_REDIRECT':
                window.location.replace(response.right);
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.appendTo($signInForm);
                $inputs.parent().addClass('has-error');
                $respTag.addClass('alert alert-danger');
                break;
        }
    });

});