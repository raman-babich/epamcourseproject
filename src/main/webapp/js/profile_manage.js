$(document).ready(function() {

    var changeNameForm = '#changeNameForm';
    var changeAvatarForm = '#changeAvatarForm';
    var resetAvatarForm = '#resetAvatarForm';
    var changeLoginForm = '#changeLoginForm';
    var changeEmailForm = '#changeEmailForm';
    var changePasswordForm = '#changePasswordForm';
    var respName = '#changeNamePanel .response';
    var respLogin = '#changeLoginPanel .response';
    var respEmail = '#changeEmailPanel .response';
    var respPassword = '#changePasswordPanel .response';
    var respAvatar = '#changeAvatarPanel .response';

    var $changeNameForm = $(changeNameForm);
    var $changeAvatarForm = $(changeAvatarForm);
    var $resetAvatarForm = $(resetAvatarForm);
    var $changeLoginForm = $(changeLoginForm);
    var $changeEmailForm = $(changeEmailForm);
    var $changePasswordForm = $(changePasswordForm);

    $(changeNameForm).change(function () {
        $(respName).remove();
    });
    $(changeLoginForm).change(function () {
        $(respLogin).remove();
    });
    $(changeEmailForm).change(function () {
        $(respEmail).remove();
    });
    $(changePasswordForm).change(function () {
        $(respPassword).remove();
    });
    $(changeAvatarForm).change(function () {
        $(respAvatar).remove();
    });

    $changeNameForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $(respName).remove();
        switch (response.left) {
            case 'OK':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($changeNameForm.parent());
                $('#profileNames').text(response.right[1] + ' ' + response.right[2]);
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($changeNameForm.parent());
                break;
        }
    });

    $changeLoginForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $(respLogin).remove();
        switch (response.left) {
            case 'HANDLE':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($changeLoginForm.parent());
                break;
            case 'OK':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($changeLoginForm.parent());
                $('#profileLogin').text(response.right[1]);
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($changeLoginForm.parent());
                break;
        }
    });

    $changeEmailForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $(respEmail).remove();
        switch (response.left) {
            case 'HANDLE':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($changeEmailForm.parent());
                break;
            case 'OK':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($changeEmailForm.parent());
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($changeEmailForm.parent());
                break;
        }
    });

    $changePasswordForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        var $inputs = $('#changeOldPassword, #changeNewPassword, #changeNewRepeatedPassword');
        $(respPassword).remove();
        switch (response.left) {
            case 'HANDLE':
                $.each(response.right.invalidParams, function (index, value) {
                    var inputSelector = 'input[name=' + value + ']';
                    var $input = $(inputSelector);
                    $input.val('');
                });
                $respTag.text(response.right.message);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($changePasswordForm.parent());
                break;
            case 'OK':
                $respTag.text(response.right.message);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($changePasswordForm.parent());
                $inputs.val('');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($changePasswordForm.parent());
                break;
        }
    });

    $changeAvatarForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        var $profileImage = $("#profileImage");
        $(respAvatar).remove();
        switch (response.left) {
            case 'HANDLE':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($changeAvatarForm.parent());
                break;
            case 'OK':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($changeAvatarForm.parent());
                var date = new Date();
                var src = $profileImage.attr('src');
                $profileImage.attr('src', src  + '&date=' + date.getTime());
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($changeAvatarForm.parent());
                break;
        }
    });

    $resetAvatarForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        var $profileImage = $("#profileImage");
        $(respAvatar).remove();
        switch (response.left) {
            case 'OK':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($changeAvatarForm.parent());
                var date = new Date();
                var src = $profileImage.attr('src');
                $profileImage.attr('src', src + '&date=' + date.getTime());
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($changeAvatarForm.parent());
                break;
        }
    });

});