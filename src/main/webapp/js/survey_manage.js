$(document).ready(function() {

    var MIN_ANSWER_INDEX = 1;
    var MAX_ANSWER_INDEX = 9;

    var addSurveyForm = '#addSurveyForm';
    var addQuestionButton = '#addQuestionButton';
    var removeQuestionButton = '#removeQuestionButton';
    var surveyQuestions = '#surveyQuestions';
    var surveyQuestion = '#surveyQuestion-0';
    var questionAmount = '#surveyQuestionAmount';

    var currentQuestionIndex = 0;
    var mainList = [1];

    var $addSurveyForm = $(addSurveyForm);

    function createQuestion(index) {
        var $clone = $(surveyQuestion).clone();
        var $selectTopic = $clone.find('#surveyQuestionTopic-0');
        newId = 'surveyQuestionTopic-' + index;
        newName = newId;
        $selectTopic.attr('id', newId);
        $selectTopic.attr('name', newName);
        var newId = 'surveyQuestion-' + index;
        $clone.attr('id', newId);
        var newName = 'surveyQuestionText-' + index;
        var $textarea = $clone.find('textarea');
        $textarea.attr('name', newName);
        $textarea.val('');
        var $addAnswerButton = $clone.find('#addAnswer-0');
        newId = 'addAnswer-' + index;
        $addAnswerButton.attr('id', newId);
        var $removeAnswerButton = $clone.find('#removeAnswer-0');
        newId = 'removeAnswer-' + index;
        $removeAnswerButton.attr('id', newId);
        var $questionAnswer = $clone.find('#questionAnswer-0');
        newId = 'questionAnswer-' + index;
        $questionAnswer.attr('id', newId);
        var $questionAnswerInput0 = $clone.find('#' + newId + ' input[name=questionAnswer-0-0]').clone();
        newName = 'questionAnswer-' + index + '-0';
        $questionAnswerInput0.attr('name', newName);
        $questionAnswerInput0.val('');
        var $questionAnswerInput1 = $clone.find('#' + newId + ' input[name=questionAnswer-0-1]').clone();
        newName = 'questionAnswer-' + index + '-1';
        $questionAnswerInput1.attr('name', newName);
        $questionAnswerInput1.val('');
        $questionAnswer.find('input').remove();
        $questionAnswerInput0.appendTo($questionAnswer);
        $questionAnswerInput1.appendTo($questionAnswer);
        var $questionAnswerAmount = $clone.find('#questionAnswerAmount-0');
        newId = 'questionAnswerAmount-' + index;
        newName = newId;
        $questionAnswerAmount.attr('id', newId);
        $questionAnswerAmount.attr('name', newName);
        $questionAnswerAmount.attr('value', 2)
        newName = 'surveyQuestionAnswerType-' + index;
        var $radio = $clone.find('input[type="radio"]');
        $radio.attr('name', newName);
        mainList.push(1);
        return $clone;
    }

    function createAnswer(questionIndex, answerIndex) {
        var $cloneAnswer = $('#questionAnswer-0').find('input[name=questionAnswer-0-0]').clone();
        $cloneAnswer.attr('name', 'questionAnswer-' + questionIndex + '-' + answerIndex);
        $cloneAnswer.val('')
        return $cloneAnswer;
    }

    $('#addSurveyForm input, #addSurveyForm textarea, #addSurveyForm').change(function () {
        $('.response').remove();
    });

    $addSurveyForm.ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $('.response').remove();
        switch(response.left) {
            case 'HANDLE':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($(addSurveyForm));
                break;
            case 'OK':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($(addSurveyForm));
                $addSurveyForm.find('input[type=text], textarea').val('');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($(addSurveyForm));
                break;
        }
    });

    $(document).on('click', 'button[id^=addAnswer-]', function () {
        var questionIndex = $(this).attr('id').substr('addAnswer-'.length);
        var answerIndex = mainList[questionIndex];
        if (answerIndex < MAX_ANSWER_INDEX) {
            createAnswer(questionIndex, answerIndex + 1).appendTo($('#questionAnswer-' + questionIndex));
            mainList[questionIndex] += 1;
            $('#questionAnswerAmount-' + questionIndex).attr('value', mainList[questionIndex] + 1);
        }
    });

    $(document).on('click', 'button[id^=removeAnswer-]', function () {
        var questionIndex = $(this).attr('id').substr('removeAnswer-'.length);
        var answerIndex = mainList[questionIndex];
        if (answerIndex > MIN_ANSWER_INDEX) {
            $('#questionAnswer-' + questionIndex + ' ' + 'input[name=questionAnswer-' + questionIndex + '-' + answerIndex + ']').remove();
            mainList[questionIndex] -= 1;
            $('#questionAnswerAmount-' + questionIndex).attr('value', mainList[questionIndex] + 1);
        }
    });

    $(addQuestionButton).on('click', function () {
        currentQuestionIndex += 1;
        createQuestion(currentQuestionIndex).appendTo($(surveyQuestions));
        $(questionAmount).attr('value', currentQuestionIndex + 1);
    });

    $(removeQuestionButton).on('click', function () {
        if (currentQuestionIndex >= 1) {
            var id = '#surveyQuestion-' + currentQuestionIndex;
            $(id).remove();
            currentQuestionIndex -= 1;
            $(questionAmount).attr('value', currentQuestionIndex + 1);
        }
    });

    var notAccessibleForm = 'form[id^="notAccessibleForm-"]';
    var accessibleForm = 'form[id^="accessibleForm-"]';

    var $mainForm;

    $(accessibleForm).on('click', function () {
        $mainForm = $(this);
    });

    $(notAccessibleForm).on('click', function () {
        $mainForm = $(this);
    });

    $(notAccessibleForm).ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $('.response').remove();
        switch(response.left) {
            case 'OK':
                var $parent = $mainForm.parent().parent();
                var $parentClone = $parent.clone();
                $parent.remove();
                var id = $parentClone.find('form input[name="id"]');
                var $form = $parentClone.find('form');
                $form.attr('id', 'notAccessibleForm-' + id);
                $form.find('input[name="value"]').attr('value', 'true');
                $parentClone.appendTo('#accessibleSurveys');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($(addSurveyForm));
                break;
        }
    });

    $(accessibleForm).ajaxForm(function (response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $('.response').remove();
        switch(response.left) {
            case 'OK':
                var $parent = $mainForm.parent().parent();
                var $parentClone = $parent.clone();
                $parent.remove();
                var id = $parentClone.find('form input[name="id"]');
                var $form = $parentClone.find('form');
                $form.attr('id', 'notAccessibleForm-' + id);
                $form.find('input[name="value"]').attr('value', 'false');
                $parentClone.appendTo('#notAccessibleSurveys');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($(addSurveyForm));
                break;
        }
    });


});