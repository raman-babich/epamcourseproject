$(document).ready(function() {

    var addTopicForm = '#addTopicForm';
    var editTopicForm = '#editTopicForm';
    var removeTopicForm = '#removeTopicForm';

    $(addTopicForm).change(function () {
        $('.response').remove();
    });
    $(editTopicForm).change(function () {
        $('.response').remove();
    });
    $(removeTopicForm).change(function () {
        $('.response').remove();
    });

    $(addTopicForm).ajaxForm(function(response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $('.response').remove();
        switch(response.left) {
            case 'HANDLE':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($(addTopicForm));
                break;
            case 'OK':
                $respTag.text(response.right);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($(addTopicForm));
                $('#addTopic').val('');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($(addTopicForm));
                break;
        }
    });

    $(editTopicForm).ajaxForm(function(response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $('.response').remove();
        switch(response.left) {
            case 'HANDLE':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($(editTopicForm));
                break;
            case 'OK':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($(editTopicForm));
                $('#editTopicSelect').find('option[value=' + response.right[1] + ']').text(response.right[2]);
                $('#editTopicInput').val('');
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($(editTopicForm));
                break;
        }
    });

    $(removeTopicForm).ajaxForm(function(response) {
        var respSelector = '<div class="response"></div>';
        var $respTag = $(respSelector);
        $('.response').remove();
        switch(response.left) {
            case 'HANDLE':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-warning');
                $respTag.appendTo($(removeTopicForm));
                break;
            case 'OK':
                $respTag.text(response.right[0]);
                $respTag.addClass('alert alert-success');
                $respTag.appendTo($(removeTopicForm));
                $('#removeTopicSelect').find('option[value=' + response.right[1] + ']').remove();
                break;
            case 'LOCATION_GO':
                window.location = response.right;
                break;
            case 'ERROR':
                $respTag.text(response.right.causeMessage + ' ' + response.right.toDoMessage);
                $respTag.addClass('alert alert-danger');
                $respTag.appendTo($(removeTopicForm));
                break;
        }
    });

});