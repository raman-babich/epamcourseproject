<article class="jumbotron">
    <h1 class="text-center"><fmt:message key="label.contacts"/></h1>
    <p><fmt:message key="text.contacts"/></p>
    <div id="googleMap">
        <iframe width="100%" height="100%" frameborder="0" style="border:0"
                src="https://www.google.com/maps/embed/v1/place?q=Belarus%20Minsk%20praspiekt%20Niezalie%C5%BEnasci%206&key=AIzaSyBWhJNUnXha-Q5cQ39x6nJeVoVPpQt7c4Y " allowfullscreen></iframe>
    </div>
</article>