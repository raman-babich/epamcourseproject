<header class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-sign-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand survey-brand-logo" href="${context}/index.jsp">Surveys</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-sign-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><fmt:message key="label.lang"/><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="text-center"><a href="${context}/main?command=changeLocale&locale=en_US"><fmt:message key="label.language.english"/></a></li>
                        <li class="text-center"><a href="${context}/main?command=changeLocale&locale=ru_RU"><fmt:message key="label.language.russian"/></a></li>
                    </ul>
                </li>
            </ul>
            <srv:guest role="${visitor.role}">
                <form class="navbar-form navbar-right">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target=".sign-up-modal-lg"><fmt:message key="label.sign.up"/></button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target=".sign-in-modal-lg"><fmt:message key="label.sign.in"/></button>
                </form>
            </srv:guest>
            <srv:notguest role="${visitor.role}">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><fmt:message key="label.profile"/><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li class="text-center"><strong id="profileNames">${srv:nullCheck(account.firstName)} ${srv:nullCheck(account.secondName)}</strong></li>
                            <li role="separator" class="divider"></li>
                            <li class="text-center"><strong id="profileLogin">${srv:nullCheck(account.login)}</strong></li>
                            <li role="separator" class="divider"></li>
                            <li class="text-center"><img id="profileImage" class="img-circle" src="${context}/main?command=loadImage&id=${account.accountId}&src=account" alt="Avatar"></li>
                            <li role="separator" class="divider"></li>
                            <li class="text-center"><a href="${context}/jsp/user/profile_settings.jsp"><fmt:message key="label.settings"/></a></li>
                            <li class="text-center"><a href="${context}/main?command=signOut"><fmt:message key="label.sign.out"/></a></li>
                        </ul>
                    </li>
                    <li><a href="${context}/jsp/user/about.jsp"><fmt:message key="label.about"/></a></li>
                    <li><a href="${context}/jsp/user/contacts.jsp"><fmt:message key="label.contacts"/></a></li>
                </ul>
                <srv:admin role="${visitor.role}">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><fmt:message key="label.surveys"/><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="text-center"><a href="${context}/jsp/admin/add_survey.jsp"><fmt:message key="label.add"/></a></li>
                                <li role="separator" class="divider"></li>
                                <li class="text-center"><a href="${context}/jsp/admin/accessible_surveys.jsp"><fmt:message key="label.list"/></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><fmt:message key="label.topics"/><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="text-center"><a href="${context}/jsp/admin/add_topic.jsp"><fmt:message key="label.add"/></a></li>
                                <li class="text-center"><a href="${context}/jsp/admin/edit_topic.jsp"><fmt:message key="label.edit"/></a></li>
                                <li class="text-center"><a href="${context}/jsp/admin/remove_topic.jsp"><fmt:message key="label.remove"/></a></li>
                                <li role="separator" class="divider"></li>
                                <li class="text-center"><a href="${context}/jsp/admin/topic_list.jsp"><fmt:message key="label.list"/></a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><fmt:message key="label.accounts"/><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="text-center"><a href="${context}/jsp/admin/account_list.jsp"><fmt:message key="label.list"/></a></li>
                            </ul>
                        </li>
                    </ul>
                </srv:admin>
            </srv:notguest>
        </div><!-- /.navbar-collapse -->
    </div>
</header>
